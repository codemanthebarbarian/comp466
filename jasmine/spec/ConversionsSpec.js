describe("UnitConverter", function(){
    var converter = {};

    beforeEach(function() {
        converter = new UnitConverter();
    });

    it("should convert foot to meter", function(done){
        var initialized = false;
        converter.loadFromFileAsync('../TMA1/part4/conversions.json').then(
        (v) => { 
            initialized = v;
            expect(initialized).toBe(true);
            expect(converter.doConversion(1, 'Foot', 'Meter')).toBe(0.3048);
            done();
        }, (v) => { 
            initialized = v;
            expect(initialized).toBe(true);
            done(); 
        });                
    });

    it("should get categories", function(done){
        var initialized = false;
        converter.loadFromFileAsync('../TMA1/part4/conversions.json').then(
        (v) => { 
            initialized = v;
            expect(initialized).toBe(true);
            expect(converter.getCategories().length).toBe(5);
            done();
        }, (v) => { 
            initialized = v;
            expect(initialized).toBe(true);
            done(); 
        });                
    });

    it("should get to length from units", function(done){
        let initialized = false;
        converter.loadFromFileAsync('../TMA1/part4/conversions.json').then(
            (v) => { 
                initialized = v;
                expect(initialized).toBe(true);
                expect(converter.getFromUnits("Length").length).toBe(2);
                done();
            }, (v) => { 
                initialized = v;
                expect(initialized).toBe(true);
                done(); 
            });
    });

    it("should get to length to units", function(done){
        let initialized = false;
        converter.loadFromFileAsync('../TMA1/part4/conversions.json').then(
            (v) => { 
                initialized = v;
                expect(initialized).toBe(true);
                expect(converter.getToUnits("Foot").length).toBe(1);
                done();
            }, (v) => { 
                initialized = v;
                expect(initialized).toBe(true);
                done(); 
            });
    });

});