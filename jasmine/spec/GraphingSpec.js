describe("GraphingCalculator", function(){
    var graphing = {};

    beforeEach(function() {
        graphing = new GraphingCalculator();
    });

    it("When undefined step is default", function(){
        graphing.set_step(undefined);
        expect(graphing.step).toBe(0.1); //default
    });

    it("When null step is default", function(){
        graphing.set_step(null);
        expect(graphing.step).toBe(0.1); //default
    });

    it("When not numeric step is default", function(){
        graphing.set_step('hello');
        expect(graphing.step).toBe(0.1); //default
    });

    it("When 1 step is updated", function(){
        graphing.set_step(1);
        expect(graphing.step).toBe(1); //default
    });

    it("When valid function is true", function(){
        expect(graphing.set_validFunction('x * 2')).toBe(true);
        expect(graphing.func).not.toBe(undefined);
    });

    it("When no x function is false", function(){
        expect(graphing.set_validFunction('z * 2')).toBe(false);
    });

    it("When no x function is undefined", function(){
        graphing.set_validFunction('z * 2');
        expect(graphing.func).toBe(undefined);
    });

    it("When not a function is false", function(){
        expect(graphing.set_validFunction('x is not a func')).toBe(false);
    });

    it("When not a function is undefined", function(){
        graphing.set_validFunction('x is not a function');
        expect(graphing.func).toBe(undefined);
    });

    it("When func is x * 2 and x is 2 then result is 4", function(){
        graphing.set_validFunction('x * 2');
        expect(graphing.func(2)).toBe(4);
    });

});