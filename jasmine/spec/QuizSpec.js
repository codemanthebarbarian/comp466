describe("QuizRepo", function(){
    var repo = {};

    beforeEach(function() {
        repo = new QuizRepo();
    });

    it("should load quiz 1 questions", function(done) {
        repo.loadFromFileAsync(
            '../TMA1/part2/exam.json'
        ).then(() => {
            expect(repo.count()).not.toBe(undefined);
            expect(repo.count()).not.toBe(null);
            expect(repo.count()).toBe(39);
            done(); 
        }).catch(() => {
            expect(0).toBe(2);
            done();
        });
    });

});