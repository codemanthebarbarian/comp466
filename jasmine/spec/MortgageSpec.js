describe('MortgageCalculator', () => {
    var sut = {}; //system under test MortgageCalculator

    beforeEach(() => {
        sut = new MortgageCalculator();
    });

    it('when default should calculate monthly payment of zero', () => {
        sut.calcPayment();
        expect(sut.pmt).toBe(0);
    });

    it('when principle only should calculate monthly payment of zero', () => {
        sut.principle = 100000.00;
        sut.calcPayment();
        expect(sut.pmt).toBe(0);
    });

    it('when zero apr, 10yr, on 120,000 should calculate monthly payment of 1000', () => {
        sut.principle = 120000.00;
        sut.years = 10;
        sut.calcPayment();
        expect(sut.pmt).toBe(1000);
    });

    it('when 1% apr, 10yr on 120,000 should calculate monthly payment of 1051.25', () => {
        sut.principle = 120000.00;
        sut.years = 10;
        sut.apr = 1.0;
        sut.calcPayment();
        expect(sut.pmt.toFixed(2)).toBe('1051.25');
    });

    it('when 2.5% apr, 10yr, on 120,000 should calculate monthly payment of 1131.24', () => {
        sut.principle = 120000.00;
        sut.years = 10;
        sut.apr = 2.5;
        sut.calcPayment();
        expect(sut.pmt.toFixed(2)).toBe('1131.24');
    });

    it('when 2.55% apr, 25yr, on 255000.75 should calculate monthly payment of 1150.41', () => {
        sut.principle = 255000.75;
        sut.years = 25;
        sut.apr = 2.55;
        sut.calcPayment();
        expect(sut.pmt.toFixed(2)).toBe('1150.41');
    });

    it('when interest changed should recalculate payment', () => {
        sut.principle = 120000.00;
        sut.years = 10;
        sut.apr = 2.5;
        sut.calcPayment();
        expect(sut.pmt.toFixed(2)).toBe('1131.24');
        sut.principle = 255000.75;
        sut.years = 25;
        sut.apr = 2.55;
        sut.calcPayment();
        expect(sut.pmt.toFixed(2)).toBe('1150.41');
    });

    it('when 2.55% apr, 25yr, on 255000.75 should generate 300 payments', () => {
        sut.principle = 255000.75;
        sut.years = 25;
        sut.apr = 2.55;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.payments.length).toBe(300);
    });

    it('when 2.55% apr, 25yr, on 255000.75 total interest 90121.57', () => {
        sut.principle = 255000.75;
        sut.years = 25;
        sut.apr = 2.55;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.totalInterest.toFixed(2)).toBe('90121.57');
    });

    it('when 2.55% apr, 25yr, on 255000.75 period 1 has 541.88 interest and 608.53 principle', () => {
        sut.principle = 255000.75;
        sut.years = 25;
        sut.apr = 2.55;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.payments[0].period).toBe(1);
        expect(sut.payments[0].interest.toFixed(2)).toBe('541.88');
        expect(sut.payments[0].principle.toFixed(2)).toBe('608.53');
    });

    it('when 2.55% apr, 25yr, on 255000.75 period 300 has 2.44 interest and 1147.97 principle', () => {
        sut.principle = 255000.75;
        sut.years = 25;
        sut.apr = 2.55;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.payments[299].period).toBe(300);
        expect(sut.payments[299].interest.toFixed(2)).toBe('2.44');
        expect(sut.payments[299].principle.toFixed(2)).toBe('1147.97');
    });

    it('when zero apr, 10yr, on 120,000 should have 120 payments', () => {
        sut.principle = 120000.00;
        sut.years = 10;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.payments.length).toBe(120);
    });

    it('when zero apr, 10yr, on 120,000 should have 0 interest', () => {
        sut.principle = 120000.00;
        sut.years = 10;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.totalInterest.toFixed(2)).toBe('0.00');
    });

    it('should regenerate amortization schedule', () => {
        sut.principle = 120000.00;
        sut.years = 10;
        sut.interest = 2.5;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.payments.length).toBe(120);
        sut.principle = 255000.75;
        sut.years = 25;
        sut.apr = 2.55;
        sut.calcPayment();
        sut.generatePayments();
        expect(sut.payments.length).toBe(300);
    });

});