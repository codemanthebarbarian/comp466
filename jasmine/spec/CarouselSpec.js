describe("ImageRepo", function(){
    var imageRepo = {};

    beforeEach(function() {
        imageRepo = new ImageRepo();
    });

    it("should load images", function(done) {
        imageRepo.loadAsync(
            '[{"image" : "Ship2.png", "title" : "x" , "description" : "x" },' + 
            '{ "image" : "Ship2.png", "title" : "y" , "description" : "y" }]'
        ).then(() => {
            expect(imageRepo.count()).not.toBe(null);
            expect(imageRepo.count()).toBe(2);
            done(); 
        }).catch(() => {
            expect(0).toBe(2);
            done();
        });
    });

    it("should move next", function(done){
        imageRepo.loadAsync(
            '[{"source" : "Ship2.png", "title" : "a" , "caption" : "a", "image": null },' + 
            '{ "source" : "Ship2.png", "title" : "b" , "caption" : "b", "image": null },' + 
            '{ "source" : "Ship2.png", "title" : "c" , "caption" : "c", "image": null }]'
        ).then(() => {
            expect(imageRepo.getCurrent().title).toBe('a');
            expect(imageRepo.getNext().title).toBe('b');
            expect(imageRepo.getNext().title).toBe('c');
            expect(imageRepo.getNext().title).toBe('a');
            done();
        }).catch(() => {
            expect(0).toBe(2);
            done();
        });
    });

    it("should move previous", function(done){
        imageRepo.loadAsync(
            '[{"source" : "Ship2.png", "title" : "a" , "caption" : "a", "image": null },' + 
            '{ "source" : "Ship2.png", "title" : "b" , "caption" : "b", "image": null },' + 
            '{ "source" : "Ship2.png", "title" : "c" , "caption" : "c", "image": null }]'
        ).then(() => {
            expect(imageRepo.getCurrent().title).toBe('a');
            expect(imageRepo.getPrevious().title).toBe('c');
            expect(imageRepo.getPrevious().title).toBe('b');
            expect(imageRepo.getPrevious().title).toBe('a');
            done();
        }).catch(() => {
            expect('0').toBe('a');
            done();
        });        
    });

    it("should load from file", function(done){
        imageRepo.loadFromFileAsync("../TMA1/part3/images/images.json")
        .then(() => {
            expect(imageRepo.count()).toBe(3);
            done();
        })
        .catch(() => {
            expect('0').toBe(3);
            done();
        });
    });

});