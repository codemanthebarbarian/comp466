describe("Util", function(){
    var util = {};

    beforeEach(function() {
        util = Util;
    });

    it("should load file", function(done) {
        util.getFile("SpecRunner.html", t => {
            expect(t).not.toBe(null);
            done();
        });        
    });

    it('should have an error (null)', function(done) {
        util.getFile("doesNotExist.html", t => {
            expect(t).toEqual(null);
            done();
        });        
    });

});