<?php
    define("public", true, true);
    
    require_once '../shared/controller.php';
    require_once './controller/usercontroller.php';
    require_once './db/userrepo.php';
    require_once '../shared/context.php';
    
    use tma2\part1\controller\UserController;
    use tma2\shared\db\Context;
    use tma2\part1\db\UserRepo;
    
    UserController::initializeSession();
    
    $controller = new UserController(new UserRepo(new Context()));
    
    $error = false;
    
    if(UserController::isPost()) {
        global $error;
        if($success = $controller->login()) {
            header("Location: ./links.php", true, 302);
            exit;
        }
        else $error = true;
    }
    
    $GLOBALS["menuItems"] = array("<a href='./home.php'>Home</a>");
    $GLOBALS[UserController::HEADER_CSS] = array("../shared/main.css","./obs.css");
    $GLOBALS[UserController::HEADER_TITLE] = "Online Bookmark System - Login";
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "../shared/view/menu.php";
?>
<form id="frm"  class="register" action="<?php echo htmlspecialchars($_SERVER["SCRIPT_NAME"]); ?>" method="post">
    <label>User Name <input id="userName" name="userName" type="text" required
        <?php if(UserController::isRegistered()) echo " value='" . $_SESSION[UserController::SESSION_USERNAME] . "' " ?>
        /></label>
    <label>Password <input id="pwd" name="pwd" type="password" required/></label>
    <input id="submit" type="submit" value="Login"/>
</form>
<?php
    if($error)
        echo "<p class='error'>Login Error - Something is not as expected.</p>";
    require_once "../shared/view/footer.php";
