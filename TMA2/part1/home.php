<?php
    define("public", true, true);
    
    require_once "../shared/controller.php";
    require_once './controller/homecontroller.php';
    require_once './db/urlrepo.php';
    require_once '../shared/context.php';
    
    use tma2\part1\controller\HomeController;
    use tma2\part1\db\UrlRepo;
    use tma2\shared\db\Context;
    
    HomeController::initializeSession();
    
    $GLOBALS[HomeController::HEADER_CSS] = array("../shared/main.css","./obs.css");
    $GLOBALS[HomeController::HEADER_TITLE] = "Online Bookmark System";

    $controller = new HomeController(new UrlRepo(new Context()));
    
    if (HomeController::isLoggedIn())
        $GLOBALS[HomeController::MENU_ITEMS] = array("<a href='./links.php'>Links</a>", "<a class='login' href='./logout.php'>Logout</a>");
    elseif ($controller->isRegistered())
        $GLOBALS[HomeController::MENU_ITEMS] = array("
    <div class='dropdown login'>
        <button class='nav-btn' onclick='window.location.href=\"./login.php\"'>Login</button>
        <div class='dropdown-items'>
            <a href='./register.php'>Register</a>
        </div>
    </div>");
    else
        $GLOBALS[HomeController::MENU_ITEMS] = array("
<div class='dropdown login'>
    <button class='nav-btn' onclick='window.location.href=\"./register.php\"'>Register</button>
    <div class='dropdown-items'>
        <a href='./login.php'>Login</a>
    </div>
</div>");
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "../shared/view/menu.php";
?>
    <p>
        Welcome
        <?php
            if(HomeController::isRegistered()){
                echo " back " . $_SESSION[HomeController::SESSION_USERNAME] . " ";
            }
        ?>
        to the online booking system.
    </p>
    <div id="top10">
        <h2>Top 10 Sites</h2>
        <ol>
            <?php
            $urls = $controller->getTop10();
            foreach ($urls as $link){
                echo "<li><a href='$link' target='_blank'>$link</a></li>\n";
            }
            ?>
        </ol>
    </div>
<?php
    require_once "../shared/view/footer.php";