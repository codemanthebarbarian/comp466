<?php

namespace tma2\part1\controller;

use tma2\shared\controller\Controller;
use tma2\part1\db\UrlRepo;

class BookmarkController extends Controller {

    private $repo;

    public function __construct(UrlRepo $repo){
        $this->repo = $repo;
    }
    
    /**
     * @param int $id
     * @return object
     */
    public function browse(int $id) : object {
        if(!$this->repo->increment($id)){
            return null;
        }
        return $this->repo->getUrlById($id);
    }
    
    /**
     * Gets the bookmarks for the logged in user.
     * @return array the user's bookmarks
     */
    public function getMyBookmarks() : array {
        if(session_status() == PHP_SESSION_NONE) session_start();
        $userId = $_SESSION[self::SESSION_USERID];
        $results = $this->repo->getByUserId($userId);
        if(sizeof($results) === 0 ) return $results;
        foreach ($results as $id => &$bookmark) {
            $bookmark["delete"] = "api/bookmarks.php";
            $bookmark["update"] = "api/bookmarks.php";
            $bookmark["browse"] = "api/browse.php";
        }
        return $results;
    }
    
    /**
     * Add s new url for the logged in in user.
     * @param string $url the url to add
     * @return object or the bookmark object
     */
    public function addBookmark(string $url) : object {
        if(session_status() == PHP_SESSION_NONE) session_start();
        $userId = $_SESSION[self::SESSION_USERID];
        if(!$this->repo->addUrl($userId, $url)) return null;
        $bookmarks = $this->repo->getUrl($userId, $url);
        if(sizeof($bookmarks) < 1) return null;
        $bookmarks[0]->delete = "api/bookmarks.php";
        $bookmarks[0]->update = "api/bookmarks.php";
        $bookmarks[0]->browse = "api/browse.php";
        return $bookmarks[0];
    }
    
    /**
     * @param string json the url object to update
     * @return object the updated url
     */
    public function updateBookmark(string $json) : object{
        $link = json_decode($json);
        $id = $this->validateId($link->id);
        if($id === 0) return null;
        if(!$this->validateBookmark($link->bookmark)) return null;
        if(!$this->repo->updateUrl($id, $link->bookmark)) $this->validationResults = array("bookmark" => "Error deleting bookmark");
        return $this->repo->getUrlById($id);
    }
    
    /**
     * deletes a url for a user
     * @param int $id the url to delete
     * @return object the deleted url
     */
    public function deleteBookmark(int $id) : object {
        if(!self::validateLogin()) return null;
        $bookmarkId = self::validateId($id);
        if($bookmarkId === 0) return null;
        if(!$this->validateOwner($bookmarkId)) return null;
        $result = $this->repo->delete($bookmarkId);
        if($result == null) $this->validationResults = array(UrlRepo::COLUMN_ID => "Error deleting url");
        return $result;
    }
    
    /**
     * @param $urlId the url to verify
     * @return bool is the current session owns the url
     */
    public function validateOwner($urlId) : bool {
        $ownerId = $this->repo->getOwnerId($urlId);
        if($ownerId === 0 ) return false;
        if(!session_status() === PHP_SESSION_ACTIVE) session_start();
        $userId = $_SESSION[self::SESSION_USERID];
        if($ownerId === $userId) return true;
        $this->validationResults = array(self::SESSION_USERNAME, "Forbidden");
        return false;
    }
    
    /**
     * Validates the provided url for the user. Check if the url is valid url and if the url
     * does not already exist for the user. Use getValidationResults() to get any validation errors.
     * @param string $url to validate
     * @return bool if the url is valid
     */
    public function validateBookmark(string $url) : bool {
        if(!filter_var($url, FILTER_VALIDATE_URL)) {
            $this->validationResults = array("url" => "Invalid URL Provided.");
            return false;
        }
        if(session_status() == PHP_SESSION_NONE) session_start();
        $userId = $_SESSION[self::SESSION_USERID];
        if($this->repo->getUrl($userId, $url) != null) {
            $this->validationResults = array('url' => "Bookmark already exists.");
        };
        return true;
    }

}