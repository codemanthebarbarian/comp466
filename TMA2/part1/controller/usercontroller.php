<?php

namespace tma2\part1\controller;

use tma2\shared\controller\Controller;
use tma2\part1\db\UserRepo;

class UserController extends Controller {

    private $repo;

    public function __construct(UserRepo $repo) {
        $this->repo = $repo;
    }
    
    /**
     * Registers a new user account in the system.
     * @return bool true if the user has successfully registered in the system
     */
    public function register(): bool{
        $userName = trim($_POST["userName"]);
        $firstNmae = trim($_POST["firstName"]);
        $lastName = trim($_POST["lastName"]);
        $pwd1 = trim($_POST["pwd1"]);
        $pwd2 = trim($_POST["pwd2"]);

        $result = $this->validateUserName($userName);
        $result = $result & $this->validatePassword($pwd1, $pwd2);

        if($result) $result = $this->repo->addUser($userName, $firstNmae, $lastName, md5($pwd2)) === 1;

        return $result;
    }
    
    /**
     * @return bool true if the user has been logged into the system.
     */
    public function login() : bool {
        if(session_status() == PHP_SESSION_NONE) session_start();
        $userName = trim($_POST["userName"]);
        $pwd = trim($_POST["pwd"]);
        $result = $this->repo->login($userName, md5($pwd));
        if($result == 0) return false;
        setcookie(self::COOKIE_USERNAME, $userName);
        $_SESSION[self::SESSION_LOGGEDIN] = true;
        $_SESSION[self::SESSION_USERNAME] = $userName;
        $_SESSION[self::SESSION_USERID] = $result;
        setcookie(self::COOKIE_USERNAME, $userName, time() + self::HOUR, "/");
        setcookie(self::COOKIE_TIMESTAMP, date_timestamp_get(date_create()), time() + self::HOUR, "/");
        session_write_close();
        return true;
    }
    
    /**
     * Logs the currently logged in user out of the system.
     */
    public function logout() {
        if(session_status() == PHP_SESSION_NONE) session_start();
        if(isset($_SESSION[self::SESSION_LOGGEDIN])) $_SESSION[self::SESSION_LOGGEDIN] = false;
        session_write_close();
    }
    
    /**
     * Validates the provided user name, it cannot be blank and cannot already exist in the system.
     * @param string $userName the user name to validate
     * @return bool if the user name is valid
     */
    private function validateUserName(string $userName) : bool {
        if($valid = empty($userName)) $this->validationResults["userName"] = "User name is required";
        if($valid &&  $valid = $this->repo->getUserName($userName) > 0)
            $this->validationResults["userName"] = "User name already exists";
        return !$valid;
    }
    
    /**
     * Validates the passwords used for validation, the passwords muss match.
     * @param string $pwd1 first password provided
     * @param string $pwd2 second password provided
     * @return bool if the passwords match
     */
    private function validatePassword(string $pwd1, string $pwd2) : bool {
        $valid = $pwd1 === $pwd2;
        if(!$valid) $this->validationResults["pwd2"] = "Passwords do not match.";
        return $valid;
    }

}