<?php

namespace tma2\part1\controller;

use tma2\shared\controller\Controller;
use tma2\part1\db\UrlRepo;

/**
 * Class HomeController
 * @package tma2\part1\controller
 */
class HomeController extends Controller {

    private $urlRepo;

    public function __construct(UrlRepo $uilRepo){
        $this->urlRepo = $uilRepo;
    }
    
    /**
     * @return array
     */
    public function getTop10() : array {
        return $this->urlRepo->getTop(10);
    }

    public function getUserLinks(){
        return $this->urlRepo->getByUser("lee");
    }
    
}
