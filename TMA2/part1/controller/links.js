"use strict";

(function () {

    let inputSave = document.getElementById("save");
    let inputVerify = document.getElementById("verify");
    let inputBookmark = document.getElementById("bookmark");
    let tableBookmarks = document.getElementById("links");
    let frmAddBookmark = document.getElementById('addBookmark');

    function verifyBookmark() {
        if(! frmAddBookmark) return false;
        return doVerify(inputBookmark);
    }

    function verifyUpdate(caller) {
        let row = document.getElementById(caller.target.dataset.id);
        let input = row.querySelector("input");
        if (input) return doVerify(input);
    }

    function doVerify(input){
        if (! input.checkValidity()) return;
        let url = input.value;
        window.open(url);
        return false; //just verify, don't clear the form
    }

    function browse(caller){
        fetch(caller.target.dataset.api, {
            method:'PUT',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/text'
            },
            body: caller.target.dataset.id
        }).then( (result) => {
            if(result.ok){
                return result.json();
            }
        }).then((json) => {
            onBrowse(json)
        });
    }

    function onBrowse(json){
        let id = json.id;
        let row = document.getElementById(id);
        row.cells[1].innerText = json.timesAccessed;
        row.cells[2].innerText = json.lastAccess;
    }

    function updateLink(caller) {
        let row = document.getElementById(caller.target.dataset.id);
        if(!row.querySelector("input")) showInput(row);
        else doUpdate(row, caller.target);
        return false;
    }

    function doUpdate(row, link){
        let input = row.querySelector("input");
        if(!input.checkValidity()) return false;
        let old = row.querySelector(".browse").innerText;
        let newbookmark = input.value;
        if(old === newbookmark) {
            window.alert("No changes to save")
            return false;
        }
        if(! confirm("Are you sure you want to update (replace) the bookmark?")) return false;
        let json = JSON.parse(link.dataset.json);
        json.bookmark = newbookmark;
        fetch(link.dataset.api, {
            method: "PUT",
            credentials: "same-origin",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/text'
            },
            body: JSON.stringify(json)
        }).then((result) => {
            if(result.ok) return result.json();
        }).then((json) => {
            updateRow(json);
        });
        return false; //ignore the link
    }

    function updateRow(json){
        let row = document.getElementById(json.id);
        let browse = row.querySelector(".browse");
        let update = row.querySelector(".update");
        browse.href = json.bookmark;
        browse.innerHTML = json.bookmark;
        let data = JSON.parse(update.dataset.json);
        data.bookmark = json.bookmark;
        update.dataset.json = JSON.stringify(data);
        clearUpdate(row);
    }

    function cancelUpdate(caller) {
        return clearUpdate(document.getElementById(caller.target.dataset.id));
    }

    function clearUpdate(row) {
        let input = row.querySelector("input");
        let bookmark = row.querySelector(".browse");
        let cancel = row.querySelector(".cancel");
        let del = row.querySelector(".delete");
        let verify = row.querySelector(".verify");
        if(input) row.cells[0].removeChild(input);
        if(cancel) row.cells[3].removeChild(cancel);
        if(verify) row.deleteCell(5);
        if(bookmark.style.display === "none") bookmark.style.display = "block";
        if(del.style.display === "none") del.style.display = "block";
        return false;
    }

    function showInput(row){
        let input = row.querySelector("input");
        let bookmark = row.querySelector(".browse");
        let cancel = row.querySelector(".cancel");
        let del = row.querySelector(".delete");
        let verify = row.querySelector(".verify");
        if(!(bookmark.style.display === "none")) bookmark.style.display = "none";
        if(!input) input = document.createElement("input");
        input.type = inputBookmark.type;
        input.pattern = inputBookmark.pattern;
        input.required = inputBookmark.required;
        input.value = bookmark.textContent;
        if(!cancel) cancel = document.createElement("a");
        cancel.href = "#";
        cancel.className = "cancel";
        cancel.dataset.id = row.id;
        cancel.onclick = cancelUpdate;
        cancel.innerText = "Cancel";
        if(!verify) verify = document.createElement("a");
        verify.href = "#";
        verify.className = "verify";
        verify.dataset.id = row.id;
        verify.onclick = verifyUpdate;
        verify.innerText = "Verify";
        row.cells[0].appendChild(input);
        if(!(del.style.display === "none")) del.style.display = "none";
        row.cells[3].appendChild(cancel);
        row.insertCell(-1).appendChild(verify);
    }

    function deleteLink(caller) {
        if(! confirm("Are you sure you want to delete the bookmark?")) return false;
        fetch(caller.target.dataset.api, {
            method:"DELETE",
            credentials: "same-origin",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/text'
            },
            body: caller.target.dataset.id
        }).then((result) => {
            if(result.ok) return result.json();
        }).then((json) => {
            let index = document.getElementById(json.id).rowIndex;
            tableBookmarks.deleteRow(index);
            return false;
        });
        return false; //don't let the link browse
    }

    function saveBookmark() {
        if (! frmAddBookmark.checkValidity()) return;
        let bookmark = inputBookmark.value;
        fetch('api/bookmarks.php', {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/text'
            },
            body: bookmark
        }).then((result) => {
            if(result.ok)
                return result.json();
            doValidationError(result.json());
            return false;
        }).then((json) => {
            doAddBookmark(json);
            return true;
        }).catch((error) => {
            inputBookmark.setCustomValidity("error");
        });
        return false;
    }

    function doAddBookmark(json){
        let row = tableBookmarks.insertRow(1);
        row.setAttribute("id", json.id);
        row.insertCell().innerHTML = "<a href='".concat(json.bookmark, "' target='_blank' name='" ,json.id ,"'>", json.bookmark, "</a>");
        row.insertCell().innerText = json.timesAccessed || 0;
        row.insertCell().innerText = json.lastAccess || "-";
        document.getElementById(json.id).onclick = browse;
    }

    function doValidationError(errors){
        inputBookmark.setCustomValidity(errors);
    }

    function bindLinks(){
        let links = tableBookmarks.getElementsByTagName("a");
        for(let i = 0 ; i < links.length ; ++i) {
            if(links[i].className === "browse") links[i].onclick = browse;
            else if(links[i].className === "delete") links[i].onclick = deleteLink;
            else if(links[i].className === 'update') links[i].onclick = updateLink;
        };
    }

    inputSave.onclick = saveBookmark;
    inputVerify.onclick = verifyBookmark;

    bindLinks();

})();