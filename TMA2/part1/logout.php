<?php
    require_once "../shared/context.php";
    require_once "./db/userrepo.php";
    require_once "../shared/controller.php";
    require_once "./controller/usercontroller.php";
    use tma2\part1\controller\UserController;
    use tma2\shared\db\Context;
    use tma2\part1\db\UserRepo;
    UserController::initializeSession();
    $controller = new UserController(new UserRepo(new Context()));
    $controller->logout();
    header("Location: ./home.php", true, 302);
    exit;