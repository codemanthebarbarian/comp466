<?php

    require_once '../shared/controller.php';
    require_once './controller/bookmarkcontroller.php';
    require_once '../shared/context.php';
    require_once "./db/urlrepo.php";
    
    use tma2\part1\controller\BookmarkController;
    use tma2\shared\db\Context;
    use tma2\part1\db\UrlRepo;
    
    BookmarkController::initializeSession();
    $controller = new BookmarkController(new UrlRepo(new Context()));
    
    $links = $controller->getMyBookmarks();
    $count = count($links);
    
    $GLOBALS[BookmarkController::HEADER_CSS] = array("../shared/main.css","./obs.css");
    $GLOBALS[BookmarkController::HEADER_TITLE] = "Online Bookmark System - My Links";
    $GLOBALS[BookmarkController::MENU_ITEMS] = array("<a href='./home.php'>Home</a>","<a class='login' href='./logout.php'>Logout</a>");
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "../shared/view/menu.php";
?>
<div id="create">
    <form id="addBookmark">
        <label>Bookmark:
            <input id="bookmark" class="url" placeholder="http:// or https://" type="url" pattern="http(s)?://.*" required>
        </label>
        <button id="save" class="tooltip">Save Bookmark<span class="tiptext">Click to save the bookmark to your account.</span></button>
        <button id="verify" class="tooltip">Verify Bookmark<span class="tiptext">Click to open the bookmark in another page.</span></button>
    </form>
</div>
<div id="list">
    <?php
        echo "<table id='links'>";
        echo "<tr>
                <th>Bookmark</th><th>Times Accessed</th><th>Last Accessed</th>
              </tr>";
        foreach($links as $key => $row){
            $lastAccessed = $row[UrlRepo::COLUMN_LASTACCESS] ?: " - ";
            $rowCount = $row[UrlRepo::COLUMN_TIMESACCESSED] ?: 0;
            $url = $row[UrlRepo::COLUMN_URL];
            $delete = $row["delete"];
            $browse = $row["browse"];
            $update = $row["update"];
            $json = '{ "id": ' . $key. ' , "bookmark": "' . $url .'" }';
            echo "<tr id='$key'>" .
                "<td><a href='$url' class='browse' target='_blank' data-api='$browse' data-id='$key'>$url</a></td><td>$rowCount</td><td>$lastAccessed</td>".
                "<td><a href='#' class='delete' data-api='$delete' data-id='$key'>Delete</a></td>" .
                "<td><a href='#' class='update' data-api='$update' data-id='$key' data-json='$json'>Update</a></td>" .
                "</tr>\n";
        }
        echo "</table>";
    ?>
</div>

<?php
$GLOBALS[BookmarkController::FOOTER_JS] = "<script type='text/javascript' src='./controller/links.js'></script>";
require_once  "../shared/view/footer.php";
?>