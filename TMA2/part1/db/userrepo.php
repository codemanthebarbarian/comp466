<?php

namespace tma2\part1\db;

use PDO;
use tma2\shared\db\Context;

class UserRepo{


    const TABLENAME = 'part1users';
    const addUserFields = array('username', 'firstname', 'lastname', 'md5');
    private $context;

    public function __construct(Context $context){
        $this->context = $context;
    }

    public function getUserName(string $userName) : array {
        $query = "select userName from " . self::TABLENAME . " where username = :username";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":username", $userName, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        if($result && $statement->rowCount() > 0){
            $names = array();
            foreach ($result as $row){
                array_push($names, $row);
            }
            return $names;
        }
        return array(0);
    }

    public function addUser(string $userName, string $firstName, string $lastName, string $md5): int{
        $query = "insert into " . self::TABLENAME . " (" . implode(",", self::addUserFields )
            . ") values(:uName, :fName, :lName, :md5);";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":uName", $userName, PDO::PARAM_STR);
        $statement->bindParam(":fName", $firstName, PDO::PARAM_STR);
        $statement->bindParam(":lName", $lastName, PDO::PARAM_STR);
        $statement->bindParam(":md5", $md5, PDO::PARAM_STR_CHAR, 32);
        if($statement->execute()) return $statement->rowCount();
        return 0;
    }
    
    public function login(string $username, string $md5): int {
        $query = "select id from " . self::TABLENAME . " where username = :username and md5 = :md5 ;";
        $conn = $this->context->getConnection();
        $statement = $conn->prepare($query);
        $statement->bindParam(":username", $username, PDO::PARAM_STR);
        $statement->bindParam(":md5", $md5, PDO::PARAM_STR_CHAR, 32);
        if(!$statement->execute() || $statement->rowCount() == 0) return 0;
        $userId = $statement->fetchColumn();
        if($userId < 1) return 0;
        $query = "update " . self::TABLENAME . " set date = CURRENT_TIMESTAMP() where username = :username;";
        $statement = $conn->prepare($query);
        $statement->bindParam(":username", $username, PDO::PARAM_STR);
        $statement->execute();
        return $userId;
    }

}