<?php

namespace tma2\part1\db;

use PDO;
use tma2\shared\db\Context;

class UrlRepo
{
    private const TABLENAME = "part1bookmarks";
    private $context;
    public const COLUMN_URL = "bookmark";
    public const COLUMN_ID = "id";
    public const COLUMN_TIMESACCESSED = "timesAccessed";
    public const COLUMN_LASTACCESS = "lastAccess";
    private const COLUMN_USERID = "userId";
    private const SELECT_BYUSERID = array(self::COLUMN_ID, self::COLUMN_URL, self::COLUMN_TIMESACCESSED, self::COLUMN_LASTACCESS);
    private const INSERT_URL = array(self::COLUMN_USERID, self::COLUMN_URL);

    public function __construct(Context $context) {
        $this->context = $context;
    }
    
    /**
     * the the top ranked urls in the system. The top ranked urls are those urls saved by different users.
     * @param int $cnt the number of urls to return
     * @return array the top N urls.
     */
    public function getTop(int $cnt) : array {
        $query = "select " . self::COLUMN_URL . " from " . self::TABLENAME . " group by " . self::COLUMN_URL . " order by count(" . self::COLUMN_URL . ") desc limit $cnt;";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_COLUMN);
    }
    
    /**
     * @param int $id
     * @return object the deleted bookmark
     */
    public function delete(int $id): object {
        $bookmark = $this->getUrlById($id);
        if(!$bookmark) return $bookmark;
        $query = "delete from " . self::TABLENAME . " where " . self::COLUMN_ID ." = :id;";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        if($statement->execute()) return $bookmark;
        return null;
    }
    
    /**
     * increments the count (timesAccessed) and update time last accessed for the provided bookmark id by one
     * @param $id int the bookmark id
     * @return bool true if the count has been incremented
     */
    public function increment(int $id) : bool {
        $query = "update " . self::TABLENAME . " set " .
            self::COLUMN_TIMESACCESSED . " = COALESCE(" . self::COLUMN_TIMESACCESSED . ", 0) + 1 , " .
            self::COLUMN_LASTACCESS . " = NOW()  where " . self::COLUMN_ID . " = :id;";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        return $statement->execute();
    }
    
    /**
     * Gets all the links for a user. If the user has no links an empty array is returned.
     * The Query is called using a FETCH_UNIQUE.
     * @param int $userId
     * @return array and associative array based on link id and then the column names
     */
    public function getByUserId(int $userId): array {
        $query = "select " . implode(",", self::SELECT_BYUSERID) . " from " . self::TABLENAME .
            " where " . self::COLUMN_USERID . " = :userId order by " . self::COLUMN_TIMESACCESSED . " DESC;" ;
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":userId", $userId, PDO::PARAM_INT);
        $statement->execute();
        if($statement->rowCount() < 1) return array();
        return $statement->fetchAll(PDO::FETCH_UNIQUE);
    }
    
    /**
     * Adds a new url for the provided user.
     * @param string $userId the id of the user to add the url to
     * @param string $url the url to add
     * @return bool true if the url was added for the user, otherwise false.
     */
    public function addUrl(string $userId, string $url) : bool {
        $query = "insert into " . self::TABLENAME . " (" . implode(",", self::INSERT_URL ) . ") values(:userId, :url);";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":userId", $userId, PDO::PARAM_INT);
        $statement->bindParam(":url", $url, PDO::PARAM_STR);
        return $statement->execute();
    }
    
    /**
     * @param int $id
     * @param string $url
     * @return bool true if the url was updated
     */
    public function updateUrl(int $id, string $url) : bool {
        $query = "update " . self::TABLENAME . " set " . self::COLUMN_URL . " = :url where " . self::COLUMN_ID . " = :id;";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->bindParam(":url", $url, PDO::PARAM_STR);
        return $statement->execute();
    }
    
    /**
     * Gets a url, id, times accessed, and last access date for the provided user. This can be used to verify if a url exists for a user.
     * @param int $userId the userid
     * @param string $url the url to find
     * @return array of bookmark objects with field based on row columns
     */
    public function getUrl(int $userId, string $url) : array {
        $query = "select " . implode(',', self::SELECT_BYUSERID). " from " . self::TABLENAME .
            " where " . self::COLUMN_USERID . " = :userId and " . self::COLUMN_URL . " = :url;";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":url", $url, PDO::PARAM_STR);
        $statement->bindParam(":userId", $userId, PDO::PARAM_INT);
        $statement->execute();
        if($statement->rowCount() < 1) return array();
        return $statement->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * @param int $id
     * @return int
     */
    public function getOwnerId(int $id) : int {
        $query = "select " . self::COLUMN_USERID . " from " . self::TABLENAME . " where " . self::COLUMN_ID . " = :id;";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        if(!$statement->execute()) return 0;
        return $statement->fetchColumn();
    }
    
    /**
     * @param int $id
     * @return object
     */
    public function getUrlById(int $id) : object {
        $query = "select " . implode(',', self::SELECT_BYUSERID). " from " . self::TABLENAME .
            " where " . self::COLUMN_ID . " = :id";
        $statement = $this->context->getConnection()->prepare($query);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
        if($statement->rowCount() < 1) return null;
        return $statement->fetchObject();
    }
}
