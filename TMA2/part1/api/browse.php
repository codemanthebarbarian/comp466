<?php
    require_once "../../shared/controller.php";
    require_once '../controller/bookmarkcontroller.php';
    require_once '../../shared/context.php';
    require_once "../db/urlrepo.php";
    
    use tma2\shared\db\Context;
    use tma2\part1\db\UrlRepo;
    use tma2\part1\controller\BookmarkController;
    
    $controller = new BookmarkController(new UrlRepo(new Context()));
    $dataBody = file_get_contents("php://input");
    
    if (BookmarkController::isPut()) {
        doBrowse();
    }
    
    function doBrowse(){
        global $controller, $dataBody;
        $id = $controller->validateId($dataBody);
        if($id === 0) onValidationError();
        $result = $controller->browse($id);
        if($result == null) onError("Error browse update on bookmark");
        http_response_code(200);
        echo json_encode($result);
        exit;
    }
    
    function onValidationError(){
        global $controller;
        http_response_code(400);
        echo json_encode($controller->getValidationResults());
        exit;
    }
    
    function onError($message){
        http_response_code(500);
        echo $message;
        exit;
    }
    
    //Fallback for invalid operations
    http_response_code(400);
    