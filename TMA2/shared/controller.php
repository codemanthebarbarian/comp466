<?php
    
    namespace tma2\shared\controller;
    
    class Controller {
        public const SESSION_LOGGEDIN = "loggedIn";
        public const SESSION_USERNAME = "userName";
        public const SESSION_USERID = "userId";
        public const COOKIE_USERNAME = "userName";
        public const COOKIE_TIMESTAMP = "stamp";
        public const HEADER_CSS = "cssScripts";
        public const HEADER_JS = "javaScripts";
        public const HEADER_TITLE = "title";
        public const MENU_ITEMS = "menuItems";
        public const FOOTER_JS = "jsController";
        
        public const DAYS_30 = 2592000;
        public const DAYS_1 = 86400;
        public const HOUR = 3600;
    
        protected $validationResults = array(0);
    
        /**
         * Gets the collection of validation errors.
         * @return array any errors during validation indexed by field
         */
        public function getValidationResults(): array {
            return $this->validationResults;
        }
    
        /**
         * @return bool true if the server request is a post.
         */
        public static function isPost() : bool{ return self::isRequest("POST"); }
    
        /**
         * @return bool true if the server request is a put
         */
        public static function isPut() : bool { return self::isRequest("PUT"); }
    
        /**
         * @return bool true if the server request is a put
         */
        public static function isDelete() : bool { return self::isRequest("DELETE"); }
    
        /**
         * @return bool true if the server request is a get
         */
        public static function isGet() : bool { return self::isRequest("GET"); }
    
        /**
         * @param string $type
         * @return bool
         */
        private static function isRequest(string $type) : bool {
            return isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] === $type;
        }
    
        /**
         * @return bool true id the user is currently logged in
         */
        public static function isLoggedIn() : bool {
            if(session_status() == PHP_SESSION_NONE) session_start();
            return isset($_SESSION[self::SESSION_LOGGEDIN]) && $_SESSION[self::SESSION_LOGGEDIN];
        }
    
        /**
         * @return bool true if the current user is registered in the system.
         */
        public static function isRegistered(){
            if(session_status() == PHP_SESSION_NONE) session_start();
            return isset($_SESSION[self::SESSION_USERNAME]) && !empty($_SESSION[self::SESSION_USERNAME]);
        }
    
        /**
         * @return bool if the session provided is logged in
         */
        public function validateLogin() : bool {
            if(self::isLoggedIn()) return true;
            $this->validationResults = array(self::SESSION_USERNAME, "Unauthorized");
            return false;
        }
    
        /**
         * Parses and validates a value for a database id
         * @param $id mixed the id to validate
         * @return int the validated (and parsed) ID
         */
        public function validateId($id) : int {
            if(! filter_var($id, FILTER_VALIDATE_INT)) {
                $this->validationResults = array("id" => "Error parsing ID value of $id");
                return 0;
            }
            $result = intval($id);
            if($result < 1) {
                $result = 0;
                $this->validationResults = array('id' => "Error ID out of range");
            }
            return $result;
        }
    
        /**
         * Initializes the session
         */
        public static function initializeSession() {
            if(session_status() == PHP_SESSION_NONE) session_start();
            if(!isset($_SESSION[self::SESSION_USERNAME]))
                $_SESSION[self::SESSION_USERNAME] = isset($_COOKIE[self::COOKIE_USERNAME]) ? $_COOKIE[self::COOKIE_USERNAME] : '';
            if(!isset($_SESSION[self::SESSION_LOGGEDIN]))
                $_SESSION[self::SESSION_LOGGEDIN] = false;
            session_write_close();
        }
    }