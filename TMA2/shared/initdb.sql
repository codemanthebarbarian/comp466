
CREATE DATABASE if not exists TMA2;

-- BOOKMARK SYSTEM TABLES

/*
Bookmark utility users table
 */
CREATE TABLE IF NOT EXISTS TMA2.part1users
(
    id        INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    md5       CHAR(32) NOT NULL,
    username  VARCHAR(30) NOT NULL UNIQUE,
    firstname VARCHAR(30),
    lastname  VARCHAR(30),
    email     VARCHAR(50),
    date      TIMESTAMP
);

/*
User bookmark table
 */
CREATE TABLE IF NOT EXISTS TMA2.part1bookmarks
(
    id            INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    userId        INT UNSIGNED NOT NULL,
    bookmark      VARCHAR(2000) NOT NULL,
    timesAccessed INT,
    lastAccess    DATETIME,
    FOREIGN KEY userLinks (userId)
        REFERENCES TMA2.part1users (id)
    ON DELETE CASCADE
);

-- ONLINE LEARNING MANAGEMENT SYSTEM TABLES

/*
The learning management system users table
 */
CREATE TABLE IF NOT EXISTS TMA2.part2users
(
    id        INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    md5       CHAR(32) NOT NULL,
    username  VARCHAR(30) NOT NULL UNIQUE,
    firstname VARCHAR(30),
    lastname  VARCHAR(30),
    email     VARCHAR(50),
    date      TIMESTAMP
);

/*
The lessons in the learning management system
 */
CREATE TABLE IF NOT EXISTS TMA2.part2lessons
(
    id          INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    title       VARCHAR(50) NOT NULL UNIQUE,
    content     MEDIUMTEXT
);

/*
The quizzes in the learning management system
 */
CREATE TABLE IF NOT EXISTS TMA2.part2quizzes
(
    id          INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    lessonID    INT UNSIGNED NOT NULL,
    title       VARCHAR(50) NOT NULL UNIQUE,
    content     MEDIUMTEXT,
    FOREIGN KEY quizLesson (lessonID)
        REFERENCES TMA2.part2lessons(id)
);

/*
User progress for lessons
 */
CREATE TABLE IF NOT EXISTS TMA2.part2progress
(
    id            INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    userId        INT UNSIGNED NOT NULL,
    lessonId      INT UNSIGNED NOT NULL,
    timesAccessed INT,
    lastAccess    DATETIME,
    FOREIGN KEY userProgress (userId)
        REFERENCES TMA2.part2users (id)
    ON DELETE CASCADE,
    FOREIGN KEY userLessons (lessonId)
        REFERENCES TMA2.part2lessons (id)
);

/*
User quiz attempts
 */
CREATE TABLE IF NOT EXISTS TMA2.part2results
(
    id            INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    userId        INT UNSIGNED NOT NULL,
    quizId        INT UNSIGNED NOT NULL,
    submitted     DATETIME,
    correct       INT,
    FOREIGN KEY userResults (userId)
        REFERENCES TMA2.part2users (id)
    ON DELETE CASCADE,
    FOREIGN KEY userQuizzes (quizId)
        REFERENCES TMA2.part2quizzes (id)
);