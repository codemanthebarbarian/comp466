<?php

namespace tma2\shared\db;

use PDO;

class Context{

    private $connection;

    public function __construct() {
        try {
            require "config.php";
            $this->connection = new PDO($dsn, $username, $password, $options);
            $sql = file_get_contents( dirname(__file__) . "/initdb.sql");
            $this->connection->exec($sql);
        } catch (PDOException $ex){
            echo $sql . "<br>" . $ex->getMessage();
        }
    }

    public function __destruct()
    {
        if($this->connection != null) $this->connection = null;
    }

    public function getConnection(){
        return $this->connection;
    }

}