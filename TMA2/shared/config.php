<?php

/**
 * Configuration for database connection
 *
 */

$host       = "localhost";
$username   = "www";
$password   = "comp466";
$dbname     = "TMA2";
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);
