<?php
    
    use tma2\shared\controller\Controller;
    
    if(!defined("public") && (!Controller::isLoggedIn())){
        header("Location: ./home.php", true, 302);
        exit;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8' />
    <title><?php echo $GLOBALS[Controller::HEADER_TITLE] ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <?php
        if(isset($GLOBALS[Controller::HEADER_CSS]))
            foreach ($GLOBALS[Controller::HEADER_CSS] as $css){
                echo "<link rel='stylesheet' type='text/css' media='screen' href='" . $css . "' />\n";
            }
    ?>
    <?php
        if(isset($GLOBALS[Controller::HEADER_JS]))
            foreach ($GLOBALS[Controller::HEADER_JS] as $js){
                echo "<script type='text/javascript' src='" . $js . "'></script>\n";
            }
    ?>
</head>
<body>
<div id="content">
