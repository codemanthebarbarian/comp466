<?php

namespace tests\part1;

require_once("/home/lee/dev/comp466/TMA2/part1/controller/homecontroller.php");

use PHPUnit\Framework\TestCase;
use tma2\part1\controller\HomeController;
use tma2\part1\controller\UrlRepo;

class obshomecontrollerTest extends TestCase
{

    public function testGetTop10()
    {
        $sut = new HomeController(new UrlRepo());
        $top = $sut->getTop10();
        $this->assertCount(2, $top);
    }
}
