<?php
    require_once "../../shared/controller.php";
    require_once "../../shared/context.php";
    require_once "../model/userrepo.php";
    
    use tma2\shared\db\Context;
    use tma2\part2\model\UserRepo;
    
    $repo = new UserRepo(new Context());
    $results = null;
    
    if(isset($_GET["userName"])) getUserName($_GET["userName"]);
    
    function getUserName(string $userName){
        global $repo, $results;
        $results = $repo->$userName($userName);
    }
    
    http_response_code(200);
    
    echo json_encode($results);