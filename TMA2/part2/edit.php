<?php
    define("public", true, true);
    require_once '../shared/context.php';
    require_once "./model/lessonrepo.php";
    require_once "./model/quizrepo.php";
    require_once '../shared/controller.php';
    require_once "./controller/olmcontroller.php";
    require_once './controller/lessoncontroller.php';
    require_once "./controller/quizcontroller.php";
    require_once "./controller/emlcontroller.php";
    require_once "./controller/menucontroller.php";
    
    use tma2\shared\db\Context;
    use tma2\part2\controller\{EmlController, MenuController};
    EmlController::initializeSession();
    $context = new Context();
    $controller = new EmlController($context);
    
    $GLOBALS[EmlController::HEADER_CSS] = array("../shared/main.css", "./view/edit.css");
    $GLOBALS[EmlController::HEADER_TITLE] = "Edit";
    $GLOBALS[EmlController::MENU_ITEMS] = (new MenuController())->getMenuItems("edit.php");
    
    
    if(EmlController::isPost()){
        if($controller->addOrReplace()) header("Location: ./lesson.php");
    }
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "./view/menu.php";
    
    ?>
    <h1>Save Edit Lesson</h1>
 
    <div id="form">
    <form id="edit" method="post">
        <label>Lesson <input class="unselectable" id="lesson" type="radio" name="type" value="lesson" /></label>
        <label>Quiz <input class="unselectable" id="quiz" type="radio" name="type" value="quiz" /></label>
        <label>Title <input id="title" name="title" type="text" required/></label>
        <input id="submit" type="submit" value="Save"/>
    </form>
    </div>
    <section>
        <div class="left">
            <textarea id="lessoncontent" name="content" form="edit"></textarea>
        </div>
        <div id="help" class="right">
            <h2>Instructions</h2>
            <p>
                This allows the creation and updating of lessons and quizzes. Lessons and quizzes are identified by their
                title. Each title must be unique and titles are case sensitive. If you add a lesson or quiz and one already
                exists with the same title, it will be overwritten.
            </p>
            <p>
                Lessons and Quizzes must be formatted using the educational markup language (EML). EML is an <a href="./lesson.php?title=XML">XML</a> based
                language with some additional formatting elements. The <a target="_blank" href="./model/eml.xsd">EML XSD Schema</a>
                defines the basic structure.
            </p>
            <p>
                See the <a href="lesson.php?title=EML">lesson on EML</a> to learn how to created system content.
            </p>
        </div>
    </section>

<?php
    $GLOBALS[EmlController::FOOTER_JS] = "<script type='text/javascript' src='controller/edit.js'></script>";
    require_once "../shared/view/footer.php";