"use strict";

(function(){

    var inputUserName = document.getElementById('userName');
    var inputPwd1 = document.getElementById('pwd1');
    var inputPwd2 = document.getElementById('pwd2');
    var inputSubmit = document.getElementById('submit');
    var frm = document.getElementById("frm");

    function validateUserName(){
        let usrName = inputUserName.value;
        if(!usrName) return;
        fetch('api/username.php?userNmae='.concat(usrName))
            .then((result) => {
                return result.json();
            })
            .then((json) => {
                if (json.length > 0) inputUserName.setCustomValidity("User exits, please use a different user name.");
                else inputUserName.setCustomValidity("");
            });
    };

    function validatePassword(){
        let pwd1 = inputPwd1.value;
        let pwd2 = inputPwd2.value;
        return pwd1 === pwd2;
    }

    function doSubmit(){
        if(! validatePassword()) {
            inputPwd2.setCustomValidity("Passwords do not match.");
            return;
        }
        inputPwd2.setCustomValidity("");
        if(! inputUserName.checkValidity()) return;
        frm.submit();
    }

    inputUserName.onkeyup = validateUserName;
    inputSubmit.onclick = doSubmit;

})();