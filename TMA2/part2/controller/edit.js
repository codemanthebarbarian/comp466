"use strict";

(function(){

    let xmlParser = new DOMParser();
    let txtcontent = document.getElementById("lessoncontent");
    let isLesson = document.getElementById("lesson");
    let isQuiz = document.getElementById("quiz");
    let txtTitle = document.getElementById("title");


    function setDocType(){
        let xml = txtcontent.value;
        if(!xml) return;
        let dom = xmlParser.parseFromString(xml, "application/xml");
        let course = dom.getElementsByTagName("course");
        if( course.length > 0) txtTitle.value = course[0].textContent.trim();
        if(dom.getElementsByTagName("lesson").length > 0) return (isLesson.checked = true);
        if(dom.getElementsByTagName("quiz").length > 0) return (isQuiz.checked = true);
        isLesson.checked = false;
        isLesson.checked = false;
    }

    txtcontent.oninput = setDocType;

})();