<?php
    
    
    namespace tma2\part2\controller;
    
    use tma2\shared\db\Context;
    use tma2\part2\model\{LessonRepo, QuizRepo};
    
    
    class EmlController extends OlmController {
        
        private $context;
        private $lessonRepo;
        
        public function __construct(Context $context) {
            $this->context = $context;
            $this->lessonRepo = new LessonRepo($this->context);
        }
        
        public function addOrReplace(){
            $type = $_POST["type"];
            switch ($type) {
                case "lesson":
                    return $this->addOrReplaceLesson();
                case "quiz":
                    return $this->addOrReplaceQuiz();
            }
            return 0;
        }
        
        public function addOrReplaceLesson() : int {
            $controller = new LessonController($this->context);
            return $controller->addOrReplace();
        }
        
        public function addOrReplaceQuiz() : int {
            $controller = new QuizController($this->context);
            $title = $_POST["title"];
            $lessonID = $this->lessonRepo->getIdByTitle($title);
            return $controller->addOrReplace($lessonID);
        }
        
    }