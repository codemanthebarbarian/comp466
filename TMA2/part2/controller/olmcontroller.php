<?php
    
    
    namespace tma2\part2\controller;
    
    use tma2\shared\controller\Controller;

    class OlmController extends Controller {
        public const SESSION_LOGGEDIN = "loggedInOlm";
        public const SESSION_USERNAME = "olmUserName";
        public const SESSION_USERID = "olmUserId";
        public const COOKIE_USERNAME = "olmUserName";
    
        /**
         * @return bool true id the user is currently logged in
         */
        public static function isLoggedIn() : bool {
            if(session_status() == PHP_SESSION_NONE) session_start();
            return isset($_SESSION[self::SESSION_LOGGEDIN]) && $_SESSION[self::SESSION_LOGGEDIN];
        }
    
        /**
         * @return bool true if the current user is registered in the system.
         */
        public static function isRegistered(){
            if(session_status() == PHP_SESSION_NONE) session_start();
            return isset($_SESSION[self::SESSION_USERNAME]) && !empty($_SESSION[self::SESSION_USERNAME]);
        }
    }