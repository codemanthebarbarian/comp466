<?php
    
    
    namespace tma2\part2\controller;
    
    use tma2\part2\model\{EmlLessonHandler, LessonRepo, EmlParser, ProgressRepo};
    use tma2\shared\db\Context;


    class LessonController extends OlmController {
        
        private $context;
        private $repo;
        
        public function __construct(Context $context) {
            $this->context = $context;
            $this->repo = new LessonRepo($context);
        }
        
        public function getLessons() : string {
            $titles = $this->repo->getTitles();
            if(count($titles)){
                $body = "";
                foreach ($titles as $title) {
                    $body .= "<p><a href='./lesson.php?title=$title'>$title</a></p>";
                }
                return $body;
            }
            return "<p>Currently there are no lessons.</p>";
        }
        
        public function getLesson(string $lesson) : string {
            $eml = $this->repo->getByTitle($lesson);
            $parser = new EmlParser(new EmlLessonHandler());
            $html = $parser->toHtml($eml);
            if(self::isLoggedIn()) $this->recordProgress($lesson);
            return $html;
        }
        
        private function recordProgress(string $lesson) {
            if(session_status() == PHP_SESSION_NONE) session_start();
            $userId = $_SESSION[self::SESSION_USERID];
            if($userId < 1) return;
            $lessonId = $this->repo->getIdByTitle($lesson);
            if($lessonId < 1) return;
            $progressRepo = new ProgressRepo($this->context);
            $progressId = $progressRepo->getId($userId, $lessonId);
            if($progressId === 0) return $progressRepo->saveAccess($userId, $lessonId);
            return $progressRepo->recordAccess($progressId);
        }
        
        public function addOrReplace() : bool {
            $title = $_POST["title"];
            $lesson = $_POST["content"];
            $id = $this->repo->getIdByTitle($title);
            if($id) return $this->repo->update($id, $title, $lesson);
            $id = $this->repo->save($title, $lesson);
            return $id > 0;
        }
    
    }