<?php
    
    
    namespace tma2\part2\controller;
    
    use tma2\shared\db\Context;
    use tma2\part2\model\{LessonRepo, QuizRepo};
    
    class MenuController extends OlmController {
        
        private $context;
        private $lessonRepo;
        private $quizRepo;
        
        public function __construct() {
            $this->context = new Context();
        }
    
        public function getMenuItems(string $page) : array {
            switch ($page){
                case "home.php":
                    return $this->getHomeItems();
                case "quiz.php":
                case "lesson.php":
                    return $this->getTutorialItems();
                default:
                    return array("<a href='home.php'>Home</a>");
            }
        }
        
        private function getTutorialItems() : array {
            $this->lessonRepo = new LessonRepo($this->context);
            $this->quizRepo = new QuizRepo($this->context);
            $titles = $this->lessonRepo->getTitles();
            $tutorials = "<div class='dropdown'><button class='nav-btn'>Tutorials</button><div class='dropdown-items'>";
            foreach ($titles as $title){
                $tutorials .= "<a href='./lesson.php?title=$title'>$title</a>";
            }
            $tutorials .= "</div></div>";
            $titles = $this->quizRepo->getTitles();
            $quizzes = "<div class='dropdown'><button class='nav-btn'>Quizzes</button><div class='dropdown-items'>";
            foreach ($titles as $title){
                $quizzes .= "<a href='./quiz.php?title=$title'>$title</a>";
            }
            $quizzes .= "</div></div>";
            return array("<a href='home.php'>Home</a>", $tutorials, $quizzes, "<a href='edit.php'>Add/Edit</a>");
        }
        
        private function getHomeItems() : array {
            $items = array("<a href='lesson.php'>Lessons</a>");
            if(self::isLoggedIn()) {
                array_push($items, "<a class='login' href='./logout.php'>Logout</a>");
            }else if (self::isRegistered()){
                array_push($items,
                    "
                    <div class='dropdown login'>
                        <button class='nav-btn' onclick='window.location.href=\"./login.php\"'>Login</button>
                        <div class='dropdown-items'>
                            <a href='./register.php'>Register</a>
                        </div>
                    </div>");
            } else {
                array_push($items,
                    "
                    <div class='dropdown login'>
                        <button class='nav-btn' onclick='window.location.href=\"./register.php\"'>Register</button>
                        <div class='dropdown-items'>
                            <a href='./login.php'>Login</a>
                        </div>
                    </div>");
            }
            return $items;
        }
        
    }