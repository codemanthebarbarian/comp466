<?php
    
    
    namespace tma2\part2\controller;
    
    use tma2\part2\model\{EmlQuizHandler, QuizRepo, EmlParser, ResultRepo};
    use tma2\shared\db\Context;

    class QuizController extends OlmController {
    
        private $repo;
        private $context;
    
        public function __construct(Context $context) {
            $this->context = $context;
            $this->repo = new QuizRepo($context);
        }
        
        public function markQuiz() : string {
            $quiz = $_POST["title"];
            $answers = array();
            $question = 1;
            while (isset($_POST[$question])){
                $$question= $_POST[$question];
                $answers[$question] = $$question;
                ++$question;
            }
            $handler = new EmlQuizHandler();
            $handler->setSubmitted($answers);
            $results = $this->parseQuiz($quiz, $handler);
            if(self::isLoggedIn()) {
                $correct = $handler->getCorrect();
                $outOf = $handler->getNumberQuestions();
                $mark = intval(($correct / $outOf) * 100);
                $this->saveResult($mark, $quiz);
            }
            return $results;
        }
        
        private function saveResult(int $result, string $quiz) {
            $quizId = $this->repo->getIdByTitle($quiz);
            if($quizId < 1) return;
            if(session_status() == PHP_SESSION_NONE) session_start();
            $userId = $_SESSION[self::SESSION_USERID];
            if($userId < 1) return;
            $resultRepo = new ResultRepo($this->context);
            $resultRepo->saveReuslt($userId, $quizId, $result);
        }
    
        public function getQuizzes() : string {
            $titles = $this->repo->getTitles();
            if(count($titles)){
                $body = "";
                foreach ($titles as $title) {
                    $body .= "<p><a href='./quiz.php?title=$title'>$title</a></p>";
                }
                return $body;
            }
            return "<p>Currently there are no quizzes.</p>";
        }
    
        public function getQuiz(string $quiz) : string {
            return $this->parseQuiz($quiz, new EmlQuizHandler());
        }
        
        private function parseQuiz(string $quiz, EmlQuizHandler $handler) : string{
            $eml = $this->repo->getByTitle($quiz);
            $parser = new EmlParser($handler);
            $html = $parser->toHtml($eml);
            return $html;
        }
    
        public function addOrReplace(int $lessonID) : bool {
            $title = $_POST["title"];
            $quiz = $_POST["content"];
            $id = $this->repo->getIdByTitle($title);
            if($id) return $this->repo->update($id, $title, $quiz, $lessonID);
            $id = $this->repo->save($title, $quiz, $lessonID);
            return $id > 0;
        }
        
    }