<?php
    define("public", true, true);
    require_once "./model/emlparser.php";
    require_once "./model/emlhandler.php";
    require_once "./model/emllessonhandler.php";
    require_once '../shared/context.php';
    require_once './model/progressrepo.php';
    require_once "./model/lessonrepo.php";
    require_once "./model/quizrepo.php";
    require_once '../shared/controller.php';
    require_once "./controller/olmcontroller.php";
    require_once './controller/lessoncontroller.php';
    require_once "./controller/menucontroller.php";
    
    use tma2\shared\db\Context;
    use tma2\part2\controller\{LessonController,MenuController};
    LessonController::initializeSession();
    $GLOBALS[LessonController::HEADER_CSS] = array("../shared/main.css");
    $GLOBALS[LessonController::HEADER_TITLE] = "Lessons";
    $context = new Context();
    $controller = new LessonController($context);
    $menuController = new MenuController();
    $GLOBALS[MenuController::MENU_ITEMS] = $menuController->getMenuItems("lesson.php");
    
    $body = "";
    
    if(LessonController::isGet() && isset($_GET["title"])){
        $body = $controller->getLesson($_GET["title"]);
    } else {
        $body = $controller->getLessons();
    }

    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "./view/menu.php";
    
    echo $body;
    
    require_once "../shared/view/footer.php";
    
    