<?php
    
    
    namespace tma2\part2\model;
    
    
    abstract class EmlHandler {
    
        protected $startOperations;
        protected $endOperations;
        protected $dataOperations;
        protected $currentElement;
        protected $currentAttributes;
        
        public function getStartOperations() : array {
            return $this->startOperations;
        }
    
        public function getEndOperations() : array {
            return $this->endOperations;
        }
    
        public function getDataOperations() : array {
            return $this->dataOperations;
        }
        
        public function setCurrentElement(string $element) {
            $this->currentElement = $element;
        }
        
        public function setCurrentAttributes(array $attributes){
            $this->currentAttributes = $attributes;
        }
    
        public function writeParagraphTagStart() {
            return "<p>";
        }
    
        public function writeParagraphTagEnd() {
            return "</p>";
        }
    
        public function writeString(string $string){
            return $string;
        }
    
        public function writeNoOp(){
            return "";
        }
    }