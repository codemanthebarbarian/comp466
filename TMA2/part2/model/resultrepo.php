<?php
    
    namespace tma2\part2\model;
    
    use PDO;
    use tma2\shared\db\Context;
    
    class ResultRepo {
        
        public const TABLENAME = "part2results";
        public const COLUMN_ID = "id";
        public const COLUMN_USERID = "userId";
        public const COLUMN_QUIZID = "quizId";
        public const COLUMN_SUBMITTED = "submitted";
        public const COLUMN_CORRECT = "correct";
        private const INSERT = array ('userId', 'quizId', 'correct', 'submitted');
        
        private $context;
        
        public function __construct(Context $context) {
            $this->context = $context;
        }
        
        public function saveReuslt(int $userId, int $quizId, int $mark) : bool {
            $query = "insert into " . self::TABLENAME . " (" . implode(",", self::INSERT) . ") " .
            "values (:userId, :quizId, :mark, NOW() );";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":userId", $userId, PDO::PARAM_INT );
            $statement->bindParam(":quizId", $quizId, PDO::PARAM_INT);
            $statement->bindParam(":mark", $mark, PDO::PARAM_INT);
            return $statement->execute();
        }
        
        public function getUserResults(int $userId) : array {
            $columns = array(self::TABLENAME . "." . self::COLUMN_ID, QuizRepo::COLUMN_TITLE , self::COLUMN_CORRECT, self::COLUMN_SUBMITTED);
            $query = "select " . implode(",", $columns) . " from " . self::TABLENAME .
                " inner join " . QuizRepo::TABLENAME . " on " . self::COLUMN_QUIZID . " = " . QuizRepo::TABLENAME . "." . QuizRepo::COLUMN_ID .
                " where " . self::COLUMN_USERID . " = :userId ;";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":userId", $userId, PDO::PARAM_INT);
            if(!$statement->execute()) return array();
            return $statement->fetchAll(PDO::FETCH_UNIQUE);
        }
    }