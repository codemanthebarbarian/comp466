<?php
    
    namespace tma2\part2\model;
    
    class EmlLessonHandler extends EmlHandler {
        
        protected $startOperations = array(
            "lesson" => "writeNoOp",
            "topic" => "writeNoOp",
            "course" => "handleCourseStart",
            "title" => "handleTitleStart",
            "link" => "handleLinkStart",
            "example" => "handleExampleStart",
            "footnotes" => "handleFootnotesStart",
            "footnote" => "handleFootnoteStart",
            "footnotetitle" => "handleFootnoteTitleStart"
        );
        
        protected $endOperations = array(
            "lesson" => "writeNoOp",
            "topic" => "writeNoOp",
            "course" => "handleCourseEnd",
            "title" => "handleTitleEnd",
            "link" => "handleLinkEnd",
            "example" => "handleExampleEnd",
            "footnotes" => "handleFootnotesEnd",
            "footnote" => "handleFootnoteEnd",
            "footnotetitle" => "handleFootnoteTitleEnd"
        );
        
        protected $dataOperations = array(
            "topic" => "writeString",
            "footnotes" => "writeNoOp",
            "link" => "handleLinkData",
            "content" => "handleContentData",
            "example" => "handleExampleData",
            "introduction" => "handleContentData"
        );
        
        public function handleCourseStart() : string {
            return "<h1>";
        }
    
        public function handleCourseEnd() : string {
            return "</h1>";
        }
    
        public function handleTitleStart() : string {
            $id = $this->currentAttributes["id"];
            return "<h3 id='$id'>";
        }
    
        public function handleTitleEnd() : string {
            return "</h3>";
        }
        
        public function handleLinkStart() : string {
            $type = $this->currentAttributes["type"];
            $link = $this->currentAttributes["to"];
            switch ($type) {
                case "self":
                    return "<a href='#$link'>";
                case "footnote":
                    return "<a href='#fn$link'>";
                default:
                    return "<a href='$link' target='_blank'>";
            }
        }
        
        public function handleLinkData($data) : string {
            if($this->currentAttributes["type"] === "footnote")
                return "<sup>$data</sup>";
            return $data;
            
        }
        
        public function handleLinkEnd() : string {
            return "</a>";
        }
        
        public function handleContentData($data) : string {
            $parts = explode("`", $data);
            $len = sizeof($parts);
            $parsed = $parts[0];
            for ($i = 1 ; $i < $len ; ++$i){
                if($i % 2 === 0) $parsed .= $parts[$i];
                else $parsed .= "<code>" . $parts[$i] . "</code>";
            }
            $parsed = str_replace("[[", "&lt;", $parsed);
            $parsed = str_replace("]]", "&gt;", $parsed);
            $parsed = str_replace("~~", "<br>", $parsed);
            $parsed = str_replace("**.", "<ul>", $parsed);
            $parsed = str_replace(".**", "</ul>", $parsed);
            $parsed = str_replace("**#", "<ol>", $parsed);
            $parsed = str_replace("#**", "</ol>", $parsed);
            $parsed = str_replace("**+", "<li>", $parsed);
            $parsed = str_replace("+**", "</li>", $parsed);
            $parsed = str_replace("''", "&quot;", $parsed);
            $parsed = str_replace("~+~", "&amp;", $parsed);
            return $parsed;
        }
    
        public function handleExampleData($data) : string {
            $parsed = str_replace("[[", "&lt;", $data);
            $parsed = str_replace("]]", "&gt;", $parsed);
            return $parsed;
        }
        
        public function handleExampleStart() : string {
            return "</p><pre><code>";
        }
        
        public function handleExampleEnd() : string {
            return "</code></pre><p>";
        }
        
        public function handleFootnotesStart() : string {
            return "<div class='$this->currentElement'>";
        }
        
        public function handleFootnotesEnd() : string {
            return "</div>";
        }
        
        public function handleFootnoteStart() : string {
            $id = $this->currentAttributes["id"];
            return "<p id='fn$id' class='$this->currentElement' >$id. ";
        }
        
        public function handleFootnoteEnd() : string {
            return "</p>";
        }
        
        public function handleFootnoteTitleStart() : string {
            return "<span class='$this->currentElement'>";
        }
        
        public function handleFootnoteTitleEnd() : string {
            return "</span>";
        }
    
    }