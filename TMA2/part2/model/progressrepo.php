<?php
    
    namespace tma2\part2\model;
    
    use PDO;
    use tma2\shared\db\Context;
    
    class ProgressRepo {
    
        public const TABLENAME = "part2progress";
        public const COLUMN_ID = "id";
        public const COLUMN_USERID = "userId";
        public const COLUMN_LESSONID = "lessonId";
        public const COLUMN_TIMESACCESSED = "timesAccessed";
        public const COLUMN_LASTACCESS = "lastAccess";
        private const INSERT = array ('userId', 'lessonId', 'timesAccessed', 'lastAccess');
        
        private $context;
        
        public function __construct(Context $context) {
            $this->context = $context;
        }
        
        public function getId(int $userId, int $lessonId) : int {
            $query = "select " . self::COLUMN_ID . " from " . self::TABLENAME .
                " where " . self::COLUMN_USERID . " = :userId and " . self::COLUMN_LESSONID . " = :lessonId ;";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":userId", $userId, PDO::PARAM_INT);
            $statement->bindParam(":lessonId", $lessonId, PDO::PARAM_INT);
            if(!$statement->execute()) return 0;
            return $statement->fetchColumn();
        }
    
        public function saveAccess(int $userId, int $lessonId) : bool {
            $query = "insert into " . self::TABLENAME . " (" . implode(",", self::INSERT) . ") " .
                "values (:userId, :lessonId, 1, NOW());";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":userId", $userId, PDO::PARAM_INT );
            $statement->bindParam(":lessonId", $lessonId, PDO::PARAM_INT);
            return $statement->execute();
        }
        
        public function recordAccess(int $progressId) : bool {
            $query = "update " . self::TABLENAME . " set " . self::COLUMN_TIMESACCESSED . " = " . self::COLUMN_TIMESACCESSED . " + 1 , " .
                self::COLUMN_LASTACCESS . " = NOW() where " . self::COLUMN_ID . " = :id ;";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":id", $progressId, PDO::PARAM_INT);
            return $statement->execute();
        }
        
        public function getUserProgress(int $userId) : array {
            $fields = array( self::TABLENAME . "." . self::COLUMN_ID, LessonRepo::COLUMN_TITLE, self::COLUMN_TIMESACCESSED, self::COLUMN_LASTACCESS);
            $query = "select " . implode(",", $fields) . " from " . self::TABLENAME . " inner join " .
                LessonRepo::TABLENAME . " on " . self::COLUMN_LESSONID . " = " . LessonRepo::TABLENAME . "." . LessonRepo::COLUMN_ID .
                " where " . self::COLUMN_USERID . " = :userId";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":userId", $userId, PDO::PARAM_INT);
            if(! $statement->execute()) return array();
            return $statement->fetchAll(PDO::FETCH_UNIQUE);
        }
        
    }