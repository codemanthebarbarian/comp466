<?php
    
    
    namespace tma2\part2\model;
    
    class EmlParser {
        
        protected $parser;
        private $handler;
        protected $html;
        protected $currentElement;
    
        public function __construct(EmlHandler $handler) {
            $this->handler = $handler;
            $this->parser = xml_parser_create();
            $this->currentElement = array();
            xml_set_object($this->parser, $this);
            xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);
            xml_set_element_handler($this->parser, "startElementHandler", "endElementHandler");
            xml_set_character_data_handler($this->parser, "elementDataHandler");
        }
    
        public function __destruct() {
            xml_parser_free($this->parser);
        }
    
        protected function startElementHandler($parser, $element, $attributes){
            array_push($this->currentElement, $element);
            $this->handler->setCurrentElement($element);
            $this->handler->setCurrentAttributes($attributes);
            $operation = $this->handler->getStartOperations()[$element] ?? "writeParagraphTagStart";
            $this->html .= call_user_func(array($this->handler, $operation));
        }
    
        protected function endElementHandler($parser, $element){
            $operation = $this->handler->getEndOperations()[$element] ?? "writeParagraphTagEnd";
            $this->html .= call_user_func(array($this->handler, $operation));
            array_pop($this->currentElement);
            $last = end($this->currentElement);
            $this->handler->setCurrentElement($last ? $last : "");
        }
    
        protected function elementDataHandler($parser, $data){
            $operation = $this->handler->getDataOperations()[end($this->currentElement)] ?? "writeString";
            $this->html .= call_user_func(array($this->handler, $operation), $data);
        }
    
        public function toHtml(string $eml) : string {
            xml_parse($this->parser, $eml);
            return $this->html;
        }
        
    }