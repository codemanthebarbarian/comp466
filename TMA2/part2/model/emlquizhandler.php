<?php
    
    
    namespace tma2\part2\model;
    
    class EmlQuizHandler extends EmlHandler {
        
        private $question = 0;
        private $answer = 0;
        private $answers = array();
        private $submitted;
        private $course;
        private $correct = 0;
        private $incorrect = 0;
        
        protected $startOperations = array (
            "course" => "handleCourseStart",
            "questions" => "handleQuestionsStart",
            "question" => "writeNoOp",
            "text" => "handleTextStart",
            "answers" => "writeNoOp",
            "answer" => "handleAnswerStart"
        );
        
        protected $endOperations = array (
            "course" => "handleCourseEnd",
            "questions" => "handleQuestionsEnd",
            "question" => "writeNoOp",
            "text" => "handleTextEnd",
            "answers" => "writeNoOp",
            "answer" => "handleAnswerEnd"
        );
        
        protected $dataOperations = array (
            "text" => "handleTextData",
            "course" => "handleCourseData",
            "answer" => "handleAnswerData"
        );
        
        public function getAnswers() : array {
            return $this->answers;
        }
        
        public function getCorrect() : int {
            return $this->correct;
        }
        
        public function getIncorrect() : int {
            return $this->incorrect();
        }
        
        public function getNumberQuestions() : int {
            return $this->question;
        }
        
        public function setSubmitted(array $answers) {
            $this->submitted = $answers;
        }
        
        public function handleCourseStart() : string {
            return "<h1>";
        }
        
        public function handleCourseEnd() : string {
            return "</h1>";
        }
        
        public function handleCourseData($data) : string {
           if($data) $this->course = $data;
            return $data;
        }
        
        public function handleQuestionsStart() : string {
            return "<form id='quiz' method='post'><input type='hidden' name='title' value='$this->course'>";
        }
        
        public function handleQuestionsEnd() : string {
            return "<br> <input id='mark' type='submit' value='Mark Quiz'></form>";
        }
        
        public function handleTextStart() :string {
            ++$this->question;
            $this->answer = 0;
            return "<p id='$this->question' class=$this->currentElement'>$this->question. ";
        }
        
        public function handleTextEnd() : string {
            return "</p>";
        }
        
        public function handleTextData(string $data) : string {
            $parsed = str_replace("[[", "&lt;", $data);
            $parsed = str_replace("]]", "&gt;", $parsed);
            $parsed = str_replace("''", "&quot;", $parsed);
            $parsed = str_replace("~+~", "&amp;", $parsed);
            return $parsed;
        }
        
        public function handleAnswerStart() : string {
            if(filter_var($this->currentAttributes["iscorrect"], FILTER_VALIDATE_BOOLEAN))
                $this->answers[$this->question] = $this->answer;
            $correct = $this->isCorrect();
            return "<label class='chk$correct'>";
        }
        
        private function isCorrect() : string {
            if(! $this->submitted) return "";
            if($this->answer != $this->submitted[$this->question]) return "";
            if($this->answers[$this->question] == $this->submitted[$this->question]) {
                ++$this->correct;
                return " correct";
            }
            ++$this->incorrect;
            return " wrong";
        }
        
        private function isChecked() : string {
            if(! $this->submitted) return "";
            if($this->submitted[$this->question] == $this->answer) return "checked";
            return "";
        }
        
        public function handleAnswerEnd() : string {
            $chkd = $this->isChecked();
            $str = "<input type='radio' name='$this->question' value='$this->answer' $chkd><span class='radio' ></span></label><br>";
            ++$this->answer;
            return $str;
        }
        
        public function handleAnswerData(string $data) : string {
            $char = 65 + $this->answer;
            $i = chr($char);
            $parsed = str_replace("[[", "&lt;", $data);
            $parsed = str_replace("]]", "&gt;", $parsed);
            $parsed = str_replace("''", "&quot;", $parsed);
            $parsed = str_replace("~+~", "&amp;", $parsed);
            return "$i) $parsed";
        }
        
    }