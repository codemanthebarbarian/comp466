<?php
    
    
    namespace tma2\part2\model;
    use PDO;
    use tma2\shared\db\Context;
    
    class LessonRepo  {
    
        public const TABLENAME = "part2lessons";
        public const COLUMN_ID = "id";
        public const COLUMN_TITLE = "title";
        public const COLUMN_CONTENT = "content";
        
        private $context;
        
        public function __construct(Context $context) {
            $this->context = $context;
        }
        
        public function getTitles() : array {
            $query = "select " . self::COLUMN_TITLE . " from " . self::TABLENAME . ";";
            $statement = $this->context->getConnection()->prepare($query);
            if(!$statement->execute()) return array();
            return $statement->fetchAll(PDO::FETCH_COLUMN);
        }
    
        public function getByTitle(string $title) : string {
            $query = "select " . self::COLUMN_CONTENT . " from " . self::TABLENAME .
                " where " . self::COLUMN_TITLE . " = :title ;";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":title", $title, PDO::PARAM_STR);
            if(! $statement->execute()) return "";
            return $statement->fetchColumn();
        }
        
        public function getIdByTitle(string $title) : int {
            $query = "select " . self::COLUMN_ID . " from " . self::TABLENAME . " where " . self::COLUMN_TITLE . " = :title;";
            $statemet = $this->context->getConnection()->prepare($query);
            $statemet->bindParam(":title", $title,  PDO::PARAM_STR);
            if(!$statemet->execute()) return 0;
            return $statemet->fetchColumn();
        }
        
        public function save(string $title, string $lesson) : int {
            $query = "insert into " . self::TABLENAME . " (" . self::COLUMN_TITLE . ", " . self::COLUMN_CONTENT . ") " .
                "values (:title, :content)";
            $statemt = $this->context->getConnection()->prepare($query);
            $statemt->bindParam(":title", $title, PDO::PARAM_STR);
            $statemt->bindParam(":content", $lesson, PDO::PARAM_LOB);
            if(!$statemt->execute()) return 0;
            return $this->context->getConnection()->lastInsertId();
        }
        
        public function update(int $id, string $title, string $lesson) : bool {
            $query = "update " . self::TABLENAME . " set " . self::COLUMN_TITLE . " = :title , " .
                self::COLUMN_CONTENT . " = :content where " . self::COLUMN_ID . " = :id;";
            $statement = $this->context->getConnection()->prepare($query);
            $statement->bindParam(":title", $title, PDO::PARAM_STR);
            $statement->bindParam(":content", $lesson, PDO::PARAM_LOB);
            $statement->bindParam(":id", $id, PDO::PARAM_INT);
            return $statement->execute();
        }
        
    }