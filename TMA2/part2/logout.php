<?php
    require_once "../shared/context.php";
    require_once "./model/userrepo.php";
    require_once "../shared/controller.php";
    require_once "./controller/olmcontroller.php";
    require_once "./controller/usercontroller.php";
    
    use tma2\part2\controller\UserController;
    use tma2\shared\db\Context;
    
    UserController::initializeSession();
    $controller = new UserController(new Context());
    $controller->logout();
    header("Location: ./home.php", true, 302);
    exit;