<?php
    define("public", true, true);
    
    require_once '../shared/controller.php';
    require_once "./controller/olmcontroller.php";
    require_once './controller/usercontroller.php';
    require_once './model/userrepo.php';
    require_once '../shared/context.php';
    
    use tma2\part2\controller\UserController;
    use tma2\shared\db\Context;
    
    UserController::initializeSession();
    $controller = new UserController(new Context());
    
    $error = false;
    
    if(UserController::isPost()) {
        global $error;
        if($success = $controller->login()) {
            header("Location: ./home.php", true, 302);
            exit;
        }
        else $error = true;
    }
    
    $GLOBALS["menuItems"] = array("<a href='./home.php'>Home</a>");
    $GLOBALS[UserController::HEADER_CSS] = array("../shared/main.css");
    $GLOBALS[UserController::HEADER_TITLE] = "Easy Lesson Online Press - Login";
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "../shared/view/menu.php";
?>
<form id="frm"  class="register" action="<?php echo htmlspecialchars($_SERVER["SCRIPT_NAME"]); ?>" method="post">
    <label>User Name <input id="userName" name="userName" type="text" required
        <?php if(UserController::isRegistered()) echo " value='" . $_SESSION[UserController::SESSION_USERNAME] . "' " ?>
        /></label>
    <label>Password <input id="pwd" name="pwd" type="password" required/></label>
    <input id="submit" type="submit" value="Login"/>
</form>
<?php
    if($error)
        echo "<p class='error'>Login Error - Something is not as expected.</p>";
    require_once "../shared/view/footer.php";
