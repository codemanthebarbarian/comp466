<?php
    define("public", true, true);
    
    require_once "../shared/context.php";
    require_once "./model/userrepo.php";
    require_once "../shared/controller.php";
    require_once "./controller/olmcontroller.php";
    require_once "./controller/usercontroller.php";
    
    use tma2\part2\controller\UserController;
    use tma2\shared\db\Context;

    UserController::initializeSession();
    $GLOBALS[UserController::HEADER_TITLE] = "Easy Lesson Online Press Registration";
    $GLOBALS[UserController::HEADER_CSS] = array("../shared/main.css");
    $GLOBALS[UserController::MENU_ITEMS] = array("<a href='./home.php'>Home</a>");
    
    $controller = new UserController(new Context());
    
    if(UserController::isPost()) {
        if($error = $controller->register()) {
            header("Location: ./home.php");
            die();
        }
    }
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "../shared/view/menu.php";
?>

<form id="frm" class="register" action="<?php echo htmlspecialchars($_SERVER["SCRIPT_NAME"]); ?>" method="post">
    <label>User Name <input id="userName" name="userName" type="text" required/></label>
    <label>First Name <input id="firstName" name="firstName" type="text"/></label>
    <label>Last Name <input id="lastName" name="lastName" type="text"/></label>
    <label>Password <input id="pwd1" name="pwd1" type="password" required/></label>
    <label>Password Repeat <input id="pwd2" name="pwd2" type="password" required></label>
    <input id="submit" type="submit" value="Save"/>
</form>

<?php

if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST") {
    echo "<p>Submitted Form</p>";
}

$GLOBALS[UserController::FOOTER_JS] = "<script type='text/javascript' src='controller/register.js'></script>";
require_once "../shared/view/footer.php";