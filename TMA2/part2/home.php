<?php
    define("public", true, true);
    
    require_once "../shared/controller.php";
    require_once "../shared/context.php";
    require_once "./model/userrepo.php";
    require_once "./model/lessonrepo.php";
    require_once "./model/quizrepo.php";
    require_once "./model/progressrepo.php";
    require_once "./model/resultrepo.php";
    require_once "./controller/olmcontroller.php";
    require_once "./controller/menucontroller.php";
    require_once "./controller/usercontroller.php";
    
    use tma2\part2\controller\{MenuController, UserController};
    use tma2\part2\model\{LessonRepo, QuizRepo, ProgressRepo, ResultRepo};
    use tma2\shared\db\Context;
    
    MenuController::initializeSession();
    $GLOBALS[MenuController::HEADER_CSS] = array("../shared/main.css");
    $GLOBALS[MenuController::HEADER_TITLE] = "Home";
    $menuController = new MenuController();
    $GLOBALS[MenuController::MENU_ITEMS] = $menuController->getMenuItems("home.php");
    
    $isLoggedIn = MenuController::isLoggedIn();
    $context = new Context();
    
    if($isLoggedIn) {
        global $context;
        $userController = new UserController($context);
        $progress = $userController->getProgress();
        $results = $userController->getResults();
    }
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "./view/menu.php";

    echo "<p>Welcome to the Easy Lesson Online Press</p>";

    if($isLoggedIn){
        echo "<div id='progress'>";
        if($progress){
            echo "<table>";
            echo "<tr><th>Lesson</th><th>Times Accessed</th><th>Last Access</th></tr>";
            foreach ($progress as $key => $row){
                $lesson = $row[LessonRepo::COLUMN_TITLE];
                $timesAccessed = $row[ProgressRepo::COLUMN_TIMESACCESSED];
                $lastAccess = $row[ProgressRepo::COLUMN_LASTACCESS];
                echo "<tr><td><a href='lesson.php?title=$lesson' >$lesson</a></td><td>$timesAccessed</td><td>$lastAccess</td></tr>";
            }
            echo "</table>";
        } else {
            echo "<p>Start a lesson to show progress</p>";
        }
        echo "</div>";
        echo "<div id='results'>";
        if($results){
            echo "<table>";
            echo "<tr><th>Quiz</th><th>Date Attempted</th><th>Mark</th></tr>";
            foreach ($results as $key => $row){
                $quiz = $row[QuizRepo::COLUMN_TITLE];
                $dateAttempted = $row[ResultRepo::COLUMN_SUBMITTED];
                $mark = $row[ResultRepo::COLUMN_CORRECT];
                echo "<tr><td><a href='quiz.php?title=$quiz'>$quiz</a></td><td>$dateAttempted</td><td>$mark %</td></tr>";
            }
            echo "</table>";
        } else {
            echo "<p>Take a quiz to track resuls.</p>";
        }
        echo "</div>";
    } else {
        echo <<<'EOT'
<p>Login or register to track progress and quiz results.</p>
EOT;
    }
    
    require_once "../shared/view/footer.php";
    