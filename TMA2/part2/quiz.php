<?php
    define("public", true, true);
    require_once "./model/emlparser.php";
    require_once "./model/emlhandler.php";
    require_once "./model/emlquizhandler.php";
    require_once '../shared/context.php';
    require_once "./model/quizrepo.php";
    require_once "./model/lessonrepo.php";
    require_once "./model/resultrepo.php";
    require_once '../shared/controller.php';
    require_once "./controller/olmcontroller.php";
    require_once './controller/quizcontroller.php';
    require_once "./controller/menucontroller.php";
    
    use tma2\shared\db\Context;
    use tma2\part2\model\QuizRepo;
    use tma2\part2\controller\{QuizController,MenuController};
    QuizController::initializeSession();
    $GLOBALS[QuizController::HEADER_CSS] = array("../shared/main.css");
    $GLOBALS[QuizController::HEADER_TITLE] = "Quizzes";
    $context = new Context();
    $controller = new QuizController($context);
    $menuController = new MenuController($context);
    $GLOBALS[MenuController::MENU_ITEMS] = $menuController->getMenuItems("quiz.php");
    
    if(QuizController::isPost()) {
        $body = $controller->markQuiz();
    } else if (QuizController::isGet() && isset($_GET["title"])) {
        $body = $controller->getQuiz($_GET["title"]);
    } else {
        $body = $controller->getQuizzes();
    }
    
    require_once "../shared/view/header.php";
    require_once "./view/banner.php";
    require_once "./view/menu.php";
    
    echo $body;
    
    require_once "../shared/view/footer.php";