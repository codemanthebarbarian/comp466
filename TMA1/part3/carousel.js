"use strict";

function ImageRepo () {
    this.index = 0;
 }

 ImageRepo.prototype.loadFromFile = function(file){
    Util.getFile(file, this.load.bind(this));
 };

 ImageRepo.prototype.loadFromFileAsync = function(file){
    return new Promise((resolve, reject) => {
        Util.getFileAsync(file)
            .then((json) => { this.loadAsync(json)})
            .then(() => resolve())
            .catch(() => reject());
    });
};

ImageRepo.prototype.loadAsync = function(json){
    return new Promise((resolve, reject) => {
        if(json === undefined || json === null) return reject();
        this.images = JSON.parse(json);
        let $i = this.images;
        let $x = this.index;
        if($i.length < 1) return reject();
        //cache the images
        for (let i = 0 ; i < $i.length ; i++) {
            $i[i].image = new Image();
            $i[i].image.src = $i[i].source;
        }
        return resolve();
    });
};

ImageRepo.prototype.getCurrent = function(){
    return this.images[this.index];
};

ImageRepo.prototype.count = function(){
    return this.images.length;
};

ImageRepo.prototype.getNext = function(){
    let $ = this;
    if($.images.length < 1) return;
    if ( ++$.index >= $.images.length) $.index = 0;
    return $.images[$.index];
};

ImageRepo.prototype.getPrevious = function(){
    let $ = this;
    if($.images.length < 1) return;
    if(--$.index < 0) $.index = $.images.length - 1;
    return $.images[$.index];
};

ImageRepo.prototype.getRandom = function(){
    let $ = this;
    if($.images.length < 1) return;
    $.index = Math.floor(Math.random() * $.images.length);
    return $.images[$.index];
};

function Carousel () {

    this.transitions = {
        SLIDE : 1,
        NONE : 2,
        FLIP : 3,
        GROW : 4
    };

    this.transition = this.transitions.GROW;

    this.spriteCurrent = {
        x : 0,
        y : 0,
        h : 0,
        w : 0,
        img : null
    };

};

Carousel.prototype.setTransition = function(transition){
    if(transition < 1 || transition > 4) return;
    this.transition = transition;
};

Carousel.prototype.loadImage = function(canvas, img) {

    if(canvas == undefined || canvas == null) return;

    function createSprite(){
        let ratioCanvas = canvas.width / canvas.height;
        let ratioImg = img.width / img.height;
        let h = 0;
        let w = 0;
        let x = 0;
        let y = 0;
        if(ratioCanvas < ratioImg) { // scale by width 
            w = canvas.width;
            h = img.height - ( img.height * (img.width - canvas.width) / img.width);
            y = ( canvas.height - h ) / 2;
        } else { // shrink by height
            w = img.width - ( img.width * ( img.height - canvas.height) / img.height);
            h = canvas.height;
            x = ( canvas.width - w ) / 2;
        }
        return { x: x, y: y, h: h, w: w, img: img };
    }    

    let $ = this;
    let speed = 33; //about 30 fps
    let timer = null;
    let ctx =  canvas.getContext('2d');
    let sprite = createSprite();
    let state = {};

    function swap(){
        $.spriteCurrent.img = sprite.img;
        $.spriteCurrent.x = sprite.x;
        $.spriteCurrent.y = sprite.y;
        $.spriteCurrent.h = sprite.h;
        $.spriteCurrent.w = sprite.w;
    };

    function grow(){
        state.x -= 10;
        state.y -= 10;
        state.w += 20;
        state.h += 20;
        if(state.x <= sprite.x || state.y <= sprite.y){
            clearInterval(timer);
            state.x = sprite.x;
            state.y = sprite.y;
            state.w = sprite.w;
            state.h = sprite.h;
            swap();
        }
        ctx.drawImage(sprite.img, state.x, state.y, state.w, state.h);
    };

    function shrink(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        $.spriteCurrent.x += 10;
        $.spriteCurrent.y += 10;
        $.spriteCurrent.h -= 20;
        $.spriteCurrent.w -= 20;
        if($.spriteCurrent.h <= 0 || $.spriteCurrent.y <= 0){
            clearInterval(timer);
            timer = setInterval(grow, speed);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        } else ctx.drawImage($.spriteCurrent.img, $.spriteCurrent.x, $.spriteCurrent.y, $.spriteCurrent.w, $.spriteCurrent.h);
    };

    function flipOut(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        $.spriteCurrent.x += 10;
        $.spriteCurrent.w -= 20;
        if($.spriteCurrent.x >= canvas.width || $.spriteCurrent.w <= 0){
            clearInterval(timer);
            timer = setInterval(flipIn, speed);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        } else ctx.drawImage($.spriteCurrent.img, $.spriteCurrent.x, $.spriteCurrent.y, $.spriteCurrent.w, $.spriteCurrent.h);
    };

    function flipIn(){
        state.x -= 10;
        state.w += 20;
        if(state.x <= sprite.x || state.w >= sprite.w){
            clearInterval(timer);
            state.x = sprite.x;
            state.y = sprite.y;
            state.w = sprite.w;
            state.h = sprite.h;
            swap();
        }
        ctx.drawImage(sprite.img, state.x, state.y, state.w, state.h);
    };

    function slideLeft(){
        if(!state.old){
            state.xOld -= 10;
            if(state.xOld + state.wOld <= 0){
                state.old = true;
            }
        }
        if(!state.new){
            state.xNew -= 10;
            if(state.xNew <= sprite.x){
                state.xNew = sprite.x;
                state.new = true;
            }
        }               
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if(!state.old)
            ctx.drawImage($.spriteCurrent.img, state.xOld, $.spriteCurrent.y, $.spriteCurrent.w, $.spriteCurrent.h);
        ctx.drawImage(sprite.img, state.xNew, sprite.y, sprite.w, sprite.h);
        if(state.old && state.new){
            clearInterval(timer);
            swap();
        }
    };
 
    if(img == undefined || img == null) paintError(canvas);
    sprite.img = img;
    switch($.transition){
        case this.transitions.SLIDE:
            state = {xOld: $.spriteCurrent.x, wOld: $.spriteCurrent.w, xNew: canvas.width, old: $.spriteCurrent.img === null, new: false};
            timer = setInterval(slideLeft, speed);
            break;
        case this.transitions.GROW:
            state = {shrink: $.spriteCurrent.img !== null, x: canvas.width / 2, y: canvas.height /2, w: 0, h: 0};
            timer = setInterval(state.shrink ? shrink : grow, speed);
            break;
        case this.transitions.FLIP:
            state = {flipOut: $.spriteCurrent.img !== null, x: canvas.width / 2, y: sprite.y, w: 0, h: sprite.h};
            timer = setInterval(state.flipOut ? flipOut : flipIn, speed);
            break;
        default:
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(sprite.img, sprite.x, sprite.y, sprite.w, sprite.h);
            swap();
            break;
    };
    
};

function paintError(canvas) {
    var ctx = canvas.getContext('2d');
    ctx.beginPath();
    ctx.arc(canvas.width / 2 , canvas.height , canvas.height / 3 ,
       Math.PI * 5 / 3, Math.PI * 4 / 3, true);
    ctx.stroke();
};