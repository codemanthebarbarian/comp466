(function(){
    var repository = new ImageRepo();
    var carousel = new Carousel();
    var transitions = Array.from(document.getElementsByName("transition"));
    var canvas = document.getElementById('displayCanvas');
    var title = document.getElementById('imageTitle');
    var caption = document.getElementById('caption');
    var previous = document.getElementById('previous');
    var next = document.getElementById('next');
    var auto = document.getElementById('auto');
    var random = document.getElementById('random');

    function doLoad(img){
        loadTitle(img);
        loadCaption(img);
        loadImage(img);
        setPreviousVisibility();
        setNextVisibility();
    };

    function setPreviousVisibility(){
        if(auto.checked || random.checked)
            previous.style.visibility = 'hidden';
        else
            previous.style.visibility = 'visible';
    }

    function setNextVisibility () {
        if(auto.checked)            
            next.style.visibility = 'hidden';
        else            
            next.style.visibility = 'visible';
    }

    function loadTitle(img){
        title.innerHTML = img.title;
    };
    
    function loadCaption(img){
        caption.innerHTML = img.caption;
    };

    function loadImage(img){
        carousel.loadImage(canvas, img.image);
    };

    function loadPrevious(){
        doLoad(repository.getPrevious());
    };

    function loadNext(){
        if(random.checked) doLoad(repository.getRandom())
        else doLoad(repository.getNext());
    }

    function setAuto(){
        clearInterval(carouselTimer);
        if(auto.checked)
            carouselTimer = setInterval(loadNext, 7500);
        setPreviousVisibility();
        setNextVisibility();
    };

    function randomChange() {
        setPreviousVisibility();
        setNextVisibility();
    }

    function setTransition(){
        let selection = transitions.find(selected => {
            return selected.checked;
        });
        carousel.setTransition(parseInt(selection.value));
    };

    repository.loadFromFileAsync('./images/images.json').then(() => doLoad(repository.getCurrent()));
    transitions.forEach(item => item.onclick = setTransition);
    previous.onclick = loadPrevious;
    next.onclick = loadNext;
    auto.onchange = setAuto;
    random.onchange = randomChange;
    var carouselTimer = setInterval(loadNext, 7500);
    
})();