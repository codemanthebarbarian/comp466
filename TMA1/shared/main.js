"use strict";

var Util = (function(){

  function getFile(file, callback){
    var request = new XMLHttpRequest();
    var path = window.location.href;
    request.onreadystatechange = function() {
      if (request.readyState === XMLHttpRequest.DONE){
        if(request.status === 200) callback(request.responseText);
        else callback(null);
      }
    }
    request.open('GET', file, true);
    request.send();
  }

  function getFileAsync(file) {
    return new Promise( (resolve, reject) => {
      const request = new XMLHttpRequest();
      request.open('GET', file, true);
      request.onload = () => resolve(request.responseText);
      request.onerror = () => reject(null);
      request.send();
    });
  }

  return {
    getFile : getFile,
    getFileAsync: getFileAsync
  };

}());