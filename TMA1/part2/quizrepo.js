function QuizRepo(){
    this.questions = [];
}

QuizRepo.prototype.loadFromFileAsync = function(file){
    return new Promise((resolve, reject) => {
        Util.getFileAsync(file)
            .then((json) => {this.loadAsync(json)})
            .then(() => resolve())
            .catch(() => reject());
    });
}

QuizRepo.prototype.loadAsync = function(json){
    return new Promise((resolve, reject) => {
        if(json === undefined || json === null) return reject();
        let o = JSON.parse(json);
        this.questions = o.questions;
        // o.questions.forEach(q => {
        //     this.questions.push(q);
        // });
        return resolve();
    });
};

QuizRepo.prototype.count = function(){
    //return Object.keys(this.questions).length;
    return this.questions.length;
};

QuizRepo.prototype.getQuestions = function() {
    return this.questions;
};

QuizRepo.prototype.markQuestion = function(question, answer){
    let q = this.questions[question];
    if(q === undefined || q === null) return false;
    let a = q.Answers[answer];
    if(a === undefined || q === null) return false;
    return a.IsCorrect;
}
