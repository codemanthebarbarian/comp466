(function(){

    var repository = new QuizRepo();
    var quizForm = document.getElementById('quizForm');
    var markQuiz = document.getElementById('mark');

    function loadQuestion(question, number){
        markQuiz.insertAdjacentHTML('beforebegin', 
            '<p id="'.concat(number)
                     .concat('">')
                     .concat(number + 1)
                     .concat('. ')
                     .concat(question.Text)
                     .concat('</p>')
        );
        let i = 0;
        question.Answers.forEach(a => {
            markQuiz.insertAdjacentHTML('beforebegin',
                '<label class="chk">'
                .concat(String.fromCharCode(65 + i))
                .concat(') ')
                .concat(a.Text)
                .concat('<input type="radio" name="')
                .concat(number)
                .concat('" value="')
                .concat(i++)
                .concat('"><span class="radio"></span></label><br>')                
            );
        });
        markQuiz.insertAdjacentHTML('beforebegin', '<br>');
    };

    function loadQuestions(questions){
        let i = 0;
        questions.forEach(q => {
            loadQuestion(q, i++);
        });
    };

    function doMarkQuiz(){
        let correct = 0;
        let n = repository.count()
        for(let i = 0 ; i < repository.count() ; i++){
            let answer = Array.from(document.getElementsByName(i)).find(selected => {
                return selected.checked;
            });
            if(answer == undefined || answer == null) continue;
            if(repository.markQuestion(i, parseInt(answer.value))) correct += 1;
        }
        alert('You have submitted your quiz.\n'
            .concat(correct)
            .concat(' out of ')
            .concat(repository.count()));
    }

    repository.loadFromFileAsync(questionsJSON).then(() => loadQuestions(repository.getQuestions()));
    markQuiz.onclick = doMarkQuiz;

})();