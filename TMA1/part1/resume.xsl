<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
    <head>
      <title>Resume <xsl:value-of select="resume/info/name"/></title>
      <link rel="stylesheet" type="text/css" href="./resume.css" />
    </head>
    <body>
      <h2>Resume: <xsl:value-of select="resume/info/name"/></h2>
      <address>
        <xsl:value-of select="resume/info/email"/> <br />
        <xsl:value-of select="resume/info/phone"/> <br />
      </address>
      <h3>Skills</h3>
      <ul>
        <xsl:for-each select="resume/skills/skill">
          <li><xsl:value-of select="."/></li>
        </xsl:for-each>
      </ul>
      <h3>Employment</h3>
      <xsl:for-each select="resume/employment/entry">
        <div class="twoCol">
          <p class="institution"><xsl:value-of select="institution"/>
          <span class="date"><xsl:value-of select="dateFrom"/> to <xsl:value-of select="dateTo"/></span></p>
        </div>
        <p class="title"><xsl:value-of select="title"/></p>
        <ul>
        <xsl:for-each select="detail">
          <li><xsl:value-of select="."/></li>
        </xsl:for-each>
        </ul>
      </xsl:for-each>
      <h3>Education</h3>
      <xsl:for-each select="resume/education/entry">
        <p class="institution"><xsl:value-of select="institution"/></p>
        <p class="title"><xsl:value-of select="title"/></p>
        <p><xsl:value-of select="dateCompleted"/></p>
        <ul>
        <xsl:for-each select="detail">
          <li><xsl:value-of select="."/></li>
        </xsl:for-each>
        </ul>
      </xsl:for-each>
    </body>
    </html>
  </xsl:template>
</xsl:stylesheet>