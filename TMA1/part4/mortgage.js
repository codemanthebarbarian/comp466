"use strict";

function MortgageCalculator(){
    this.apr = 0;           //annual percentage rate
    this.years = 0;         //duration years
    this.principle = 0.0;   //loan amount
    this.nr = 0.0;          //nominal rate
    this.periods = 12;      //periods per year
    this.fv = 0;            //future value (total paid)
    this.interest;          //interest paid
    this.payments = [];     //payments
    this.totalInterest = 0  //total interest paid

    // monthly payment      //                     principle * nr
    this.pmt = 0;           //  pmt = ----------------------------------------
                            //         1 - ( 1 / ( 1 + nr ) ^ years * periods

    this.payment = (n, p, i) => {
        return {
            period: n,
            principle: p,
            interest: i
        };
    };
}

MortgageCalculator.prototype.calcPayment = function() {
    if(this.years === 0) return;
    if(this.principle === 0) return;
    let n = this.years * this.periods;
    if(this.apr <= 0) { //no interest just simple calc
        this.pmt = this.principle / n;
        return;
    }
    this.nr = this.apr / this.periods / 100;
    let numerator = this.principle * this.nr;
    let denominator = 1 - ( 1 / (Math.pow(1 + this.nr, n)));
    this.pmt = numerator / denominator;
};

MortgageCalculator.prototype.generatePayments = function() {
    this.totalInterest = 0;
    while(this.payments.length) this.payments.pop();
    let pv = this.principle;
    let n = this.payments.length; //this will be zero
    while(pv > 0){
        let i = pv * this.nr;
        let p = this.pmt - i;
        pv -= p;
        n = this.payments.push(this.payment(++n, p, i));
        this.totalInterest += i;
    }
};
