"use strict";

function UnitConverter () {
    this.getConversion = function (from, to) {
        if (this.conversions === undefined || this.conversions === null) return null;
        return this.conversions.find( $c => { return $c.from === from && $c.to === to });
    }
}

UnitConverter.prototype.loadFromFile = function (file) {
    Util.getFile(file, this.load.bind(this));
};

UnitConverter.prototype.loadFromFileAsync = function(file) {
    return new Promise((resolve, reject) => {
        Util.getFileAsync(file).then(
            (json) => { 
                this.load(json);
                resolve(true);
            }, () => { reject(false);} );
    });
};

UnitConverter.prototype.load = function (json){
    this.conversions = JSON.parse(json);
    this.conversions.forEach(element => {
        element.convert = new Function("x", element.formula);
    });
};

UnitConverter.prototype.getCategories = function () {
    if(this.categories !== undefined) return this.categories;
    this.categories = [];
    this.conversions.forEach(element => {
        let x = this.categories.find(elem => {
            return elem === element.category;
        });
        if(x === undefined || x === null) this.categories.push(element.category);
    });
    return this.categories;
}

UnitConverter.prototype.getFromUnits = function(category) {
    let result = [];
    if(category === undefined || category === null) return result;
    this.conversions.forEach(element => {
        if(element.category === category) result.push(element.from);
    });
    return result;
};

UnitConverter.prototype.getToUnits = function(unit) {
    let result = [];
    if(unit === undefined || unit === null) return result;
    this.conversions.forEach(element => {
        if(element.from === unit) result.push(element.to);
    });
    return result;
};

UnitConverter.prototype.doConversion = function (val, from, to) {    
    let $ = this.getConversion(from, to);
    if ($ === undefined || $ === null) return null;
    return $.convert(val);
};