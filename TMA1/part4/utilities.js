(function(){

    var unitConverter = new UnitConverter();
    var lastScroll = 0;
    var scrollLock = false;

    var onWindowScroll = () => {
        if(scrollLock) return;
        scrollLock = true;
        let current = window.pageYOffset;
        if(current > lastScroll) windowScrollDown(current);
        else windowScrollUp(current);
    }

    var windowScrollDown = (current) => {
        let mortgageY = document.getElementById('mortgage').offsetTop;
        let graphY = document.getElementById('graphing').offsetTop;
        let to = mortgageY > current ? mortgageY : graphY
        scrollTo(0, to);
        setTimeout(() => {
            scrollLock = false;
            lastScroll = window.pageYOffset;
        } , 1000);
    }

    var windowScrollUp = (current) => {
        let mortgageY = document.getElementById('mortgage').offsetTop;
        let to = mortgageY < current ? mortgageY : 0;
        scrollTo(0,to);
        setTimeout(() => {
            scrollLock = false;
            lastScroll = window.pageYOffset;
        } , 1000);
    }

    window.onscroll = onWindowScroll;

    var unitFrom = document.getElementById("unitFrom");
    var unitTo = document.getElementById("unitTo");
    var categories = Array.from(document.getElementsByName("conversion"));
    var conversionIn = document.getElementById("conversionIn");
    var conversionOut = document.getElementById("conversionOut");

    var getSelectedCategory = () => {
        let selection = categories.find(selected => {
            return selected.checked;
        });
        setFrom(selection.value);
    };

    var setFrom = (category) => {
        let units = unitConverter.getFromUnits(category);
        while(unitFrom.options.length) unitFrom.options.remove(0);
        let i = 0;
        units.forEach( unit => {
            unitFrom.options.add(new Option(unit));
        });
        setTo();
    };

    var setTo = () => {
        let fromUnit = unitFrom.options[unitFrom.selectedIndex].value;
        while(unitTo.options.length) unitTo.options.remove(0);
        let units = unitConverter.getToUnits(fromUnit);
        if(units === undefined || units === null) return;
        let i = 0;
        units.forEach( unit => {
            unitTo.options.add(new Option(unit));
        });
    };

    var calculateConversion = () => {
        let from = conversionIn.value;
        if(from === undefined || from === null) {
            conversionOut.value = null;
            return;
        }
        let fromUnit = unitFrom.options[unitFrom.selectedIndex].value;
        let toUnit = unitTo.options[unitTo.selectedIndex].value;
        let out = unitConverter.doConversion(from, fromUnit, toUnit);
        conversionOut.value = (out === undefined || out === null) ? null : out;
    };

    var initConversions = () => {
        unitConverter.loadFromFileAsync('./conversions.json').then(getSelectedCategory);
        unitFrom.onchange = setTo;
        categories.forEach(item => item.onclick = getSelectedCategory);
        conversionIn.onchange = calculateConversion;
        conversionIn.onkeyup = calculateConversion;
    };

    initConversions();

    ///////////////////////////////////////////////////////////////////////////

    var mortgageCalculator = new MortgageCalculator();

    var mortgageAmount = document.getElementById('principle');
    var mortgageInterest = document.getElementById('interest');
    var mortgageLength = document.getElementById('duration');
    var mortgagePayment = document.getElementById('pmt');
    var mortgageInterestPaid = document.getElementById('interestPaid');
    var mortgageAmortization = document.getElementById('amortization').getElementsByTagName('tbody')[0];

    var buildAmortizationTable = () => {
        for(let i = mortgageAmortization.rows.length -1 ; i > 0 ; i--) 
            mortgageAmortization.deleteRow(i);
        let payment = mortgageCalculator.pmt.toFixed(2);
        let balance = mortgageCalculator.principle;
        mortgageCalculator.payments.forEach(p => {
            let row = mortgageAmortization.insertRow(-1);
            row.insertCell(0).innerHTML = p.period; //payment number
            row.insertCell(1).innerHTML = payment; //payment amount
            row.insertCell(2).innerHTML = p.principle.toFixed(2); //payment principle
            row.insertCell(3).innerHTML = p.interest.toFixed(2); //payment interest
            row.insertCell(4).innerHTML = (balance -= p.principle).toFixed(2); //balance
        });
    };

    var calculateMortgage = () => {
        mortgageCalculator.calcPayment();
        mortgageCalculator.generatePayments();
        mortgagePayment.value = mortgageCalculator.pmt.toFixed(2);
        mortgageInterestPaid.value = mortgageCalculator.totalInterest.toFixed(2);
        buildAmortizationTable();
    };

    var setYears = () => {
        mortgageCalculator.years = mortgageLength.options[mortgageLength.selectedIndex].value;
        calculateMortgage();
    };

    var setInterest = () => {
        let apr = mortgageInterest.value;
        let err = document.querySelector('#interest+.error');
        if(isNaN(apr) || apr < 0){
            err.innerHTML = 'Enter a value greater than zero'
            return;
        }
        err.innerHTML = '';
        mortgageCalculator.apr = apr;
        calculateMortgage();
    };

    var setPrinciple = () => {
        let p = mortgageAmount.value;
        let err = document.querySelector('#principle+.error');
        if(isNaN(p) || p < 0){
            err.innerHTML = 'Enter a value greater than zero'
            return;
        }
        err.innerHTML = '';
        mortgageCalculator.principle = p;
        calculateMortgage();
    };

    var initMortgage = () => {
        setYears();
        mortgageAmount.onkeyup = setPrinciple;
        mortgageInterest.onkeyup = setInterest;
        mortgageLength.onchange = setYears;
    };

    initMortgage();

    ///////////////////////////////////////////////////////////////////////////

    var graphingCalc = new GraphingCalculator();
    var graphView = document.getElementById('graphCanvas');
    var graphButton = document.getElementById('doGraph');
    var funcText = document.getElementById('func');

    var generateGraph = () => {
        if( ! graphingCalc.set_validFunction(funcText.value)) return;
        graphView.getContext('2d').clearRect(0, 0, graphView.width, graphView.height);
        graphingCalc.paintXScale(graphView);
        graphingCalc.paintYScale(graphView);
        graphingCalc.plot(graphView);
    };

    var initGraphing = () => {
        graphButton.onclick = generateGraph;
    };

    initGraphing();

})();