"use strict";

function GraphingCalculator(){
    this.xTo = 10;
    this.xFrom = -10;
    this.yTo = 10;
    this.yFrom = -10;
    this.step = 0.1;
    this.lineWidth = 3;
};

GraphingCalculator.prototype.set_xTo = function(xTo){
    if(xTo === undefined || xTo === null || isNaN(step)) return;
    this.xTo = xTo;
};

GraphingCalculator.prototype.set_xFrom = function(xFrom){
    if(xFrom === undefined || xFrom === null || isNaN(step)) return;
    this.xFrom = xFrom;
};

GraphingCalculator.prototype.set_yTo = function(yTo){
    if(yTo === undefined || yTo === null || isNaN(step)) return;
    this.yTo = yTo;
};

GraphingCalculator.prototype.set_yFrom = function(yFrom){
    if(yFrom === undefined || yFrom === null || isNaN(step)) return;
    this.yFrom = yFrom;
};

GraphingCalculator.prototype.set_step = function(step){
    if(step === undefined || step === null || isNaN(step)) return;
    this.step = step;
};

GraphingCalculator.prototype.set_validFunction = function(func){
    if(func === undefined || func === null) return false;
    if(func.indexOf('x') < 0) return false; //must contain an x
    try { //for the func we need the inverse (origin is at the top)
        this.func = new Function("x", "return -1 * (".concat(func).concat(")"));
        let y = this.func(this.xFrom); //test min
        y = this.func(this.xTo);   //test max
    } catch {
        this.func = undefined;
        return false;
    }
    return true;
};

GraphingCalculator.prototype.paintXScale = function(view) {
    if(view === undefined || view === null) return;
    let ctx = view.getContext('2d');
    let h = view.height / 2;
    ctx.strokeStyle = 'black';
    ctx.lineWidth = this.lineWidth;
    ctx.beginPath();
    ctx.moveTo(0, h);
    ctx.lineTo(view.width, h);
    ctx.stroke();
};

GraphingCalculator.prototype.paintYScale = function(view) {
    if(view === undefined || view === null) return;
    let ctx = view.getContext('2d');
    let x = view.width / 2;
    ctx.strokeStyle = 'black';
    ctx.lineWidth = this.lineWidth;
    ctx.beginPath();
    ctx.moveTo(x , 0);
    ctx.lineTo(x, view.height);
    ctx.stroke();
};

GraphingCalculator.prototype.plot = function(view) {
    if(this.func === undefined) return;
    if(view === undefined || view === null) return;
    let ctx = view.getContext('2d');
    let yStep = (this.yTo - this.yFrom) / view.height;
    let xStep = (this.xTo - this.xFrom) / view.width;
    let yOff = (this.func(0) * -1 ) + (view.height / 2 ); //need to offset correctly for formula as scale is inverted
    ctx.strokeStyle = 'red';
    ctx.lineWidth = this.lineWidth;
    ctx.beginPath();
    ctx.moveTo(0, this.func(this.xFrom) / yStep + yOff);
    let x = this.xFrom + xStep;
    for(let i = 1 ; i <= view.width ; i++){
        ctx.lineTo(i, this.func(x) / yStep + yOff);
        ctx.stroke();
        x += xStep;
    }
};